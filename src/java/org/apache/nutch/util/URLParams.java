package org.apache.nutch.util;

public class URLParams {

	private String url;
	 private String divClass;
	 private String titleClass;
	 private String articleEndsWith;
	 private String depth;
	 private String titleChar;
	 private String newsSource;
	 private String leadingPattern;
	 private String trailingPattern;
	 private String imagePosition;
	 private String extractor;
	 private String hubEndsWith;
	 private String classForDate;
	 private String dateLeadingPattern;
	 private String dateTrailingPattern;
	 private String idcode;
	 private String resourceType;
	 private String cleanedUrl;
	 private String unwantedRegex;
	 private String boilerpipe;
	 private String titleRegex;

	/**
	 * @param url
	 * @param electionURL
	 * @param articleEndsWith
	 * @param depth
	 * @param titleChar
	 * @param newsSource
	 * @param leadingPattern
	 * @param trailingPattern
	 * @param imagePosition
	 */
		 /**
		 * @return the hubEndsWith
		 */

	 	public String getTitleRegex() {
			return titleRegex;
		}
		public void setTitleRegex(String titleRegex) {
			this.titleRegex = titleRegex;
		}	 
	 
	 	public String getUnwantedRegex() {
			return unwantedRegex;
		}
		public void setUnwantedRegex(String unwantedRegex) {
			this.unwantedRegex = unwantedRegex;
		}

	 	public String getBoilerpipe() {
			return boilerpipe;
		}
		public void setBoilerpipe(String boilerpipe) {
			this.boilerpipe = boilerpipe;
		}

		public String getHubEndsWith() {
			return hubEndsWith;
		}
		public String getCleanedUrl() {
			return cleanedUrl;
		}
		public void setCleanedUrl(String cleanedUrl) {
			this.cleanedUrl = cleanedUrl;
		}
		public String getResourceType() {
			return resourceType;
		}
		public void setResourceType(String resourceType) {
			this.resourceType = resourceType;
		}
		/**
		 * @param hubEndsWith the hubEndsWith to set
		 */
		public void setHubEndsWith(String hubEndsWith) {
			this.hubEndsWith = hubEndsWith;
		}
		/**
		 * @return the extractor
		 */
		
		
		
		public String getExtractor() {
			return extractor;
		}
		public String getClassForDate() {
			return classForDate;
		}
		public void setClassForDate(String classForDate) {
			this.classForDate = classForDate;
		}
		public String getDateLeadingPattern() {
			return dateLeadingPattern;
		}
		public void setDateLeadingPattern(String dateLeadingPattern) {
			this.dateLeadingPattern = dateLeadingPattern;
		}
		public String getDateTrailingPattern() {
			return dateTrailingPattern;
		}
		public void setDateTrailingPattern(String dateTrailingPattern) {
			this.dateTrailingPattern = dateTrailingPattern;
		}
		public String getIdcode() {
			return idcode;
		}
		public void setIdcode(String idcode) {
			this.idcode = idcode;
		}
		/**
		 * @param extractor the extractor to set
		 */
		public void setExtractor(String extractor) {
			this.extractor = extractor;
		}
		/**
		 * @return the url
		 */
		public String getUrl() {
			return url;
		}
		/**
		 * @param url the url to set
		 */
		public void setUrl(String url) {
			this.url = url;
		}
		
		public String getDivClass() {
			return divClass;
		}
		public void setDivClass(String divClass) {
			this.divClass = divClass;
		}
		/**
		 * @return the articleEndsWith
		 */
		public String getArticleEndsWith() {
			return articleEndsWith;
		}
		/**
		 * @param articleEndsWith the articleEndsWith to set
		 */
		public void setArticleEndsWith(String articleEndsWith) {
			this.articleEndsWith = articleEndsWith;
		}
		/**
		 * @return the depth
		 */
		public String getDepth() {
			return depth;
		}
		/**
		 * @param depth the depth to set
		 */
		public void setDepth(String depth) {
			this.depth = depth;
		}
		/**
		 * @return the titleChar
		 */
		public String getTitleChar() {
			return titleChar;
		}
		/**
		 * @param titleChar the titleChar to set
		 */
		public void setTitleChar(String titleChar) {
			this.titleChar = titleChar;
		}
		/**
		 * @return the newsSource
		 */
		public String getNewsSource() {
			return newsSource;
		}
		/**
		 * @param newsSource the newsSource to set
		 */
		public void setNewsSource(String newsSource) {
			this.newsSource = newsSource;
		}
		/**
		 * @return the leadingPattern
		 */
		public String getLeadingPattern() {
			return leadingPattern;
		}
		/**
		 * @param leadingPattern the leadingPattern to set
		 */
		public void setLeadingPattern(String leadingPattern) {
			this.leadingPattern = leadingPattern;
		}
		/**
		 * @return the trailingPattern
		 */
		public String getTrailingPattern() {
			return trailingPattern;
		}
		
		/**
		 * @param trailingPattern the trailingPattern to set
		 */
		public void setTrailingPattern(String trailingPattern) {
			this.trailingPattern = trailingPattern;
		}
		/**
		 * @return the imagePosition
		 */
		public String getImagePosition() {
			return imagePosition;
		}
		
		
		public URLParams() {
			super();
		}
		/**
		 * @param imagePosition the imagePosition to set
		 */
		public void setImagePosition(String imagePosition) {
			this.imagePosition = imagePosition;
		}
		
		public String getTitleClass() {
			return titleClass;
		}
		public void setTitleClass(String titleClass) {
			this.titleClass = titleClass;
		}
		public URLParams(String url,String hubEndsWith ,String articleEndsWith, String divClass, String titleClass, String depth,
				String newsSource, String leadingPattern, String trailingPattern,
				 String classForDate,String dateLeadingPattern, String dateTrailingPattern, String idcode,String resourceType,String cleanedUrl,String unwantedRegex,String boilerpipe, String titleRegex) {
			super();
			this.url = url;
			this.divClass = divClass;
			this.titleClass = titleClass;
			this.articleEndsWith = articleEndsWith;
			this.depth = depth;
			this.newsSource = newsSource;
			this.leadingPattern = leadingPattern;
			this.trailingPattern = trailingPattern;
			this.hubEndsWith = hubEndsWith;
			this.classForDate = classForDate;
			this.dateLeadingPattern = dateLeadingPattern;
			this.dateTrailingPattern = dateTrailingPattern;
			this.idcode = idcode;
			this.resourceType=resourceType;
			this.cleanedUrl=cleanedUrl;
			this.unwantedRegex=unwantedRegex;
			this.boilerpipe=boilerpipe;
			this.titleRegex=titleRegex;

		}

		public URLParams(String url,String hubEndsWith ,String articleEndsWith, String divClass, String titleClass, String depth,
				String newsSource, String leadingPattern, String trailingPattern,
				 String classForDate,String dateLeadingPattern, String dateTrailingPattern, String idcode,String resourceType,String cleanedUrl) {
			super();
			this.url = url;
			this.divClass = divClass;
			this.titleClass = titleClass;
			this.articleEndsWith = articleEndsWith;
			this.depth = depth;
			this.newsSource = newsSource;
			this.leadingPattern = leadingPattern;
			this.trailingPattern = trailingPattern;
			this.hubEndsWith = hubEndsWith;
			this.classForDate = classForDate;
			this.dateLeadingPattern = dateLeadingPattern;
			this.dateTrailingPattern = dateTrailingPattern;
			this.idcode = idcode;
			this.resourceType=resourceType;
			this.cleanedUrl=cleanedUrl;

		}


		@Override
		public String toString() {
			return "URLParams [url=" + url + ", divClass=" + divClass + ", titleClass=" + titleClass
					+ ", articleEndsWith=" + articleEndsWith + ", depth=" + depth + ", titleChar=" + titleChar
					+ ", newsSource=" + newsSource + ", leadingPattern=" + leadingPattern + ", trailingPattern="
					+ trailingPattern + ", imagePosition=" + imagePosition + ", extractor=" + extractor
					+ ", hubEndsWith=" + hubEndsWith + ", classForDate=" + classForDate + ", dateLeadingPattern="
					+ dateLeadingPattern + ", dateTrailingPattern=" + dateTrailingPattern + ", idcode=" + idcode
					+ ", resourceType=" + resourceType + ", cleanedUrl=" + cleanedUrl + ", unwantedRegex=" + unwantedRegex + ", boilerpipe=" + boilerpipe + ", titleregex=" + titleRegex + "]";
		}



		
		
	} 
