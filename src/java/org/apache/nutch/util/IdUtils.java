/**
 * 
 */
package org.apache.nutch.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;

/**
 * @author Lovey
 * @Date June 22, 2016
 * @Description --Utility class for a URL
 * @LastModifications -- NaN
 * 
 */
public class IdUtils {

	/* List of URLS based on 2 documents in package Class */
	private static ArrayList<String> govtWebsiteDomains = new ArrayList<>();
	private static ArrayList<String> eNewsWebsite = new ArrayList<String>();
	private static SimpleDateFormat sdf;
	private static String GOVT_WEBSITES_LIST_LOCATION;
	private static String ENEWS_WEBSITES_LIST_LOCATION;
	private static String ENEWS_WEBSITES_SPECIFIC_LIST;
	private static ArrayList<String> eNewsSpecificWebsites = new ArrayList<String>();
	private static String GOVW_WEBSITES_SPECIFIC_LIST;
	private static ArrayList<String> govwSpecificWebsites = new ArrayList<String>();
	private static HashMap<String, String> EnewsIDCodeMap;
	private static HashMap<String, String> GovwIDCodeMap;
	
	public static String IDENTIFIER;
	public static String RESOURCETYPE;
	public static String IDENTIFIED_HOSTNAME;
	public static String BASEURL;
	public static String IDCODE;
	
	private static Logger LOG=Logger.getLogger("IdUtils");

	static {
		sdf = new SimpleDateFormat("yyyyddMMhhmmss");

	}

	public static void getIdentifier(String cleanedurl, String actualurl, Configuration conf) throws IOException {
		
		System.out.println("in getIdentifier at 60----2017"+conf.get("enewspapers.code.list.location")); 
		
		if (IdUtils.isEnewsWebsite(cleanedurl, conf)) {
			if(EnewsIDCodeMap==null || EnewsIDCodeMap.size()==0)
			{
				EnewsIDCodeMap=new HashMap<>();
				String line;
				BufferedReader reader = new BufferedReader(new FileReader(conf.get("enewspapers.code.list.location")));
				
				while ((line = reader.readLine()) != null) {
					String[] parts = line.split("\t");
					if (parts.length >= 2) 
					{
						String key = parts[1].trim();
						String value = parts[0].trim();
						LOG.log(Level.INFO,"key: "+key+"\t value:"+value);
						EnewsIDCodeMap.put(key, value);
					}
					else
					{
						/*LOG.log(Level.INFO,"ignoring line: " + line);*/
					}
				}

				reader.close();
			}
			
			try {
				IDCODE=getIdCode(actualurl, EnewsIDCodeMap);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			IDENTIFIER = IDCODE.equals("NOTFND")?"ENEW_" + cleanedurl:"ENEW_" + IDCODE;
			
			RESOURCETYPE="ENEW";
			

		} else if (IdUtils.isGovtWebsite(cleanedurl, conf)) {

			
			if(GovwIDCodeMap==null || GovwIDCodeMap.size()==0)
			{
				GovwIDCodeMap=new HashMap<>();
				String line;
				BufferedReader reader = new BufferedReader(new FileReader(conf.get("govwwebsites.code.list.location")));
				while ((line = reader.readLine()) != null) {
					String[] parts = line.split("\t");
					if (parts.length >= 2) 
					{
						String key = parts[1].trim();
						String value = parts[0].trim();
						LOG.log(Level.INFO,"key: "+key+"\t value:"+value);
						GovwIDCodeMap.put(key, value);
					}
					else
					{
						/*LOG.log(Level.INFO,"ignoring line: " + line);*/
					}
				}

				reader.close();
			}
			
			try {
				IDCODE=getIdCode(actualurl, GovwIDCodeMap);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			IDENTIFIER = IDCODE.equals("NOTFND")?"GOVW_" + cleanedurl:"GOVW_" + IDCODE;
			RESOURCETYPE="GOVW";

		} else {

			IDENTIFIER = "UDEF_" + cleanedurl;RESOURCETYPE="UDEF";

		}
		IDENTIFIER+="_"+sdf.format(new Date());
	}

	
	// Changes in these function required swapping of if else if snippet as per need for ENEW and GOVW websites
	public static void getHostname(String resourcetype, URL host, Configuration conf) throws URISyntaxException {
					if (resourcetype.equals("ENEW")) {
				if (eNewsSpecificWebsites.isEmpty()) {
					ENEWS_WEBSITES_SPECIFIC_LIST = conf.get("enewswebsites.list.specificenews");
					//LOG.log(Level.INFO,"ENEWS_WEBSITES_SPECIFIC_LIST:  " + ENEWS_WEBSITES_SPECIFIC_LIST);
					String[] spiltstring = ENEWS_WEBSITES_SPECIFIC_LIST.split(":");

					for (String s : spiltstring) {
						/*LOG.log(Level.INFO,"Indexing Filterchecker spiltstring: " + s);*/
						eNewsSpecificWebsites.add(s.toUpperCase());
					}
				}

				//Save baseUrl of resourceType ENEW in baseUrl field
				BASEURL=host.getHost().toUpperCase().replaceAll("WWW.", "").replaceAll(".COM", "");
				
				boolean flag = false;
				for (String s : eNewsSpecificWebsites) {
					//LOG.log(Level.INFO,"eNewsSpecificWebsites in IF 316" + s);

					if (host.getHost().toUpperCase().contains("."+s+".")) // Select Hostname
																	// from //
																	// predefined
																	// list of//
																	// urls (from
																	// nutch-site.xml)
					{
						/*LOG.log(Level.INFO,"HOST URL in IF" + host.toURI().toString());*/

						flag = true;
						IDENTIFIED_HOSTNAME = s.toUpperCase();
						break;
					}
				}
				if (!flag) {
					/*System.out
							.println("for url" + host.toURI().toString() + " flag is still false, hence 2nd if is called");
					IDENTIFIED_HOSTNAME = host.getHost().substring(0, host.getHost().indexOf('.')).toUpperCase();*/
					IDENTIFIED_HOSTNAME=host.getHost().toUpperCase().replaceAll("WWW.", "").replaceAll(".COM", "");
				}
			}	

		

		else

if (resourcetype.equals("GOVW")) {
	
	System.out.println("in getHOSt GOVW"+conf.get("govwebsites.list.specificgovt")); 
			if (govwSpecificWebsites.isEmpty()) {
				GOVW_WEBSITES_SPECIFIC_LIST = conf.get("govwebsites.list.specificgovt");
			//	LOG.log(Level.INFO,"GOVW_WEBSITES_SPECIFIC_LIST:  " + GOVW_WEBSITES_SPECIFIC_LIST);
				String[] spiltstring = GOVW_WEBSITES_SPECIFIC_LIST.split(":");

				for (String s : spiltstring) {
					/*LOG.log(Level.INFO,"Indexing Filterchecker spiltstring: " + s);*/
					govwSpecificWebsites.add(s.toUpperCase());
				}
			}
			//Save baseUrl of resourceType GOVW in baseUrl field
			BASEURL=host.getHost().toUpperCase().replaceAll("WWW.", "").replaceAll(".COM", "");
			boolean flag = false;
			for (String s : govwSpecificWebsites) {
				LOG.log(Level.INFO,"govwSpecificWebsites in IF 316" + s);

				if (host.getHost().toUpperCase().contains("."+s+".")) // Select Hostname
																// from //
																// predefined
																// list of//
																// urls (from
																// nutch-site.xml)
				{
					LOG.log(Level.INFO,"HOST URL in IF" + host.toURI().toString());

					flag = true;
					IDENTIFIED_HOSTNAME = s.toUpperCase();
					break;
				}
			}
			if (!flag) {
				//System.out.println("for url" + host.toURI().toString() + " flag is still false, hence 2nd if is called");
				
				//IDENTIFIED_HOSTNAME = host.getHost().substring(0, host.getHost().indexOf('.')).toUpperCase();
				IDENTIFIED_HOSTNAME=host.getHost().toUpperCase().replaceAll("WWW.", "").replaceAll(".COM", "");
			}
		}

		

			else
			{
				//No hostname for UDEF
				//IDENTIFIED_HOSTNAME="UNDEFINED";//why the hostname has to be UNDEFINED..it should b the hostname as it is coming in url if not find :-\
				//added by pragya on 10th Aug 2017
				IDENTIFIED_HOSTNAME=host.getHost().toUpperCase().replaceAll("WWW.", "").replaceAll(".COM", "");
				BASEURL=host.getHost().toUpperCase().replaceAll("WWW.", "").replaceAll(".COM", "");
			}

	}

	public static boolean isGovtWebsite(String url_in, Configuration conf) {
		if (govtWebsiteDomains.isEmpty()) {
			GOVT_WEBSITES_LIST_LOCATION = conf.get("govtwebsites.list");
			//LOG.log(Level.INFO,"GOVT_WEBSITES_LIST_LOCATION: " + GOVT_WEBSITES_LIST_LOCATION);
			String[] spiltstring = GOVT_WEBSITES_LIST_LOCATION.split(":");
			for (String s : spiltstring) {
				LOG.log(Level.INFO,"IDUtils GOVT_LIST spiltstring: " + s);
				govtWebsiteDomains.add(s);
			}
		}
		for (String domain : govtWebsiteDomains) {
			if (url_in.toLowerCase().contains("_" + domain.toLowerCase()))
				return true;
		}
		return false;
	}

	public static boolean isEnewsWebsite(String url_in, Configuration conf) {
		if (eNewsWebsite.isEmpty()) {
			ENEWS_WEBSITES_LIST_LOCATION = conf.get("enewswebsites.list");
			//LOG.log(Level.INFO,"ENEWS_WEBSITES_LIST_LOCATION: " + ENEWS_WEBSITES_LIST_LOCATION);
			String[] spiltstring = ENEWS_WEBSITES_LIST_LOCATION.split(":");
			for (String s : spiltstring) {
				LOG.log(Level.INFO,"IDUtils ENEWS_LIST spiltstring: " + s);
				eNewsWebsite.add(s);
			}
		}

		for (String domain : eNewsWebsite) {
			if (url_in.toLowerCase().contains(domain.toLowerCase()))
				return true;
		}
		return false;
	}

	public static String cleanUrls(URL url) {

		String cleanedurl = url.getHost() + url.getFile();
		// http://timesofindia.indiatimes.com/enews/article/121224ss12.htm
		cleanedurl = cleanedurl.replaceAll("http://", "").replaceAll("https://", "").replaceAll("www", "")
				.replaceAll("/", ".").replaceAll("_", "").replaceAll(".html", "").replaceAll(".htm", "")
				.replaceAll(".php", "").replaceAll(".jsp", "").replaceAll(".aspx", "");
		cleanedurl = cleanedurl.replace('.', '_').replaceAll("\\?", "_").replaceAll("=", "_");
		if (cleanedurl.startsWith("_")) {
			cleanedurl = cleanedurl.replaceFirst("_", "");
		}
		if (cleanedurl.endsWith("_")) {
			cleanedurl = cleanedurl.substring(0, cleanedurl.length() - 1);
		}
	
		cleanedurl = cleanedurl.toUpperCase();
		return cleanedurl;
	}

	public static URL getUrl(String url) {
		try {
			return new URL(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public static String getIdCode(String urlstring,Map<String,String> map)  throws Exception {
				
		Pattern p;
		Matcher m;
		for (String key : map.keySet()) {
			p = Pattern.compile(key, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

			m = p.matcher(urlstring);

			if (m.find()) 
			{
				/*LOG.log(Level.INFO,"for url: " + urlstring + " pattern found is "
						+ key.toString() + " setting identifier: "
						+ map.get(key));*/
				return map.get(key);
			}

		}
		return "NOTFND";

		
	}

	public static void main(String[] args) {

		String URL = "http://www.arunachalfront.info/columnist.php?topic=109";
		URL u = getUrl(URL);
		LOG.log(Level.INFO,"Passed url: " + URL);
		LOG.log(Level.INFO,"protocol = " + u.getProtocol()); // http
		LOG.log(Level.INFO,"authority = " + u.getAuthority()); // example.com:80
		LOG.log(Level.INFO,"host = " + u.getHost()); // example.com
		LOG.log(Level.INFO,"port = " + u.getPort()); // 80
		LOG.log(Level.INFO,"path = " + u.getPath()); // /docs/books/tutorial/index.html
		LOG.log(Level.INFO,"query = " + u.getQuery()); // name=networking
		LOG.log(Level.INFO,"filename = " + u.getFile()); /// docs/books/tutorial/index.html?name=networking
		LOG.log(Level.INFO,"ref = " + u.getRef()); // DOWNLOADING

		LOG.log(Level.INFO,"Clean URL=" + IdUtils.cleanUrls(u));
	}

}
