/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cdac.nvli.utility;

// Nutch imports
import org.apache.nutch.crawl.CrawlDatum;
import org.apache.nutch.crawl.Inlinks;
import org.apache.nutch.indexer.IndexingFilter;
import org.apache.nutch.indexer.IndexingException;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.hadoop.io.Text;
import org.apache.nutch.parse.Parse;
import org.apache.nutch.parse.ParseData;
import org.apache.nutch.metadata.Metadata;
import org.apache.nutch.net.protocols.HttpDateFormat;
import org.apache.nutch.net.protocols.Response;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.time.DateUtils;
// Hadoop imports
import org.apache.hadoop.conf.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tempuri.NormalizeDateLocator;
import org.tempuri.NormalizeDateSoap;

import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.NattyParser;
import com.joestelmach.natty.ParseLocation;

public class UtilityExtractorFilter implements IndexingFilter {

	private Configuration conf;
	private static NattyParser nattyParser;
	public static final Logger LOG = LoggerFactory.getLogger(UtilityExtractorFilter.class);
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private static String lastdate = "0001-01-00T00:00:01Z";
	private static long lastTime;
	public static final String EN_KEYWORDS_LIST = "enkeywords.dir.file";
	public static final String HI_KEYWORDS_LIST = "hikeywords.dir.file";
	public static final String RU_KEYWORDS_LIST = "rukeywords.dir.file";
	public static final String TE_KEYWORDS_LIST = "tekeywords.dir.file";
	public static final String PA_KEYWORDS_LIST = "pakeywords.dir.file";
	public static final String MR_KEYWORDS_LIST = "mrkeywords.dir.file";

	public static LinkedHashSet<String> enKeywords;
	public static LinkedHashSet<String> hiKeywords;
	public static LinkedHashSet<String> ruKeywords;
	public static LinkedHashSet<String> teKeywords;
	public static LinkedHashSet<String> paKeywords;
	public static LinkedHashSet<String> mrKeywords;
	static {
		try {
			lastTime = sdf.parse(lastdate).getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public UtilityExtractorFilter() {

	}

	// Inherited JavaDoc
	public NutchDocument filter(NutchDocument doc, Parse parse, Text url, CrawlDatum datum, Inlinks inlinks)
			throws IndexingException {
		String url_s = url.toString();
		try {

			addKeywords(doc, parse.getText(), url_s, datum);
			//addPublishDate(doc, parse.getData(), url_s, datum); //added article date in IndexMapReduce on 1 Nov 2017

			addTime(doc, parse.getData(), url_s, datum);

			// overrideFields(doc, parse.getData(), url_s, datum);
			urlPathDepth(url_s, doc);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return doc;
	}

	private void addKeywords(NutchDocument doc, String content, String url_s, CrawlDatum datum) {
		String enKeywordsResource = conf.get(EN_KEYWORDS_LIST);
		String hiKeywordsResource = conf.get(HI_KEYWORDS_LIST);
		String ruKeywordsResource = conf.get(RU_KEYWORDS_LIST);
		String teKeywordsResource = conf.get(TE_KEYWORDS_LIST);
		String paKeywordsResource = conf.get(PA_KEYWORDS_LIST);
		String mrKeywordsResource = conf.get(MR_KEYWORDS_LIST);

		enKeywords = getKeywordsFromResources(enKeywordsResource);
		hiKeywords = getKeywordsFromResources(hiKeywordsResource);
		ruKeywords = getKeywordsFromResources(ruKeywordsResource);
		teKeywords = getKeywordsFromResources(teKeywordsResource);
		paKeywords = getKeywordsFromResources(paKeywordsResource);
		mrKeywords = getKeywordsFromResources(mrKeywordsResource);

		System.out.println("list loaded for Keywords! :)");

		
		if (!enKeywords.isEmpty()) {
			for(String keyword:enKeywords){
				if ((content.toLowerCase()).contains(" " + keyword.toLowerCase() + " ")) {
					doc.add("domain_keyword_en", keyword.toString().toLowerCase());
				}
			}		
		}

		if (!hiKeywords.isEmpty()) {
			for(String keyword:hiKeywords){
				if ((content.toLowerCase()).contains(" " + keyword.toLowerCase() + " ")) {
					doc.add("domain_keyword_hi", keyword.toString().toLowerCase());
				}
			}		
		}
		if (!ruKeywords.isEmpty()) {
			for(String keyword:ruKeywords){
				if ((content.toLowerCase()).contains(" " + keyword.toLowerCase() + " ")) {
					doc.add("domain_keyword_ru", keyword.toString().toLowerCase());
				}
			}		
		}

		if (!teKeywords.isEmpty()) {
			for(String keyword:teKeywords){
				if ((content.toLowerCase()).contains(" " + keyword.toLowerCase() + " ")) {
					doc.add("domain_keyword_te", keyword.toString().toLowerCase());
				}
			}		
		}

		if (!paKeywords.isEmpty()) {
			for(String keyword:paKeywords){
				if ((content.toLowerCase()).contains(" " + keyword.toLowerCase() + " ")) {
					doc.add("domain_keyword_pa", keyword.toString().toLowerCase());
				}
			}		
		}

		if (!mrKeywords.isEmpty()) {
			for(String keyword:mrKeywords){
				if ((content.toLowerCase()).contains(" " + keyword.toLowerCase() + " ")) {
					doc.add("domain_keyword_mr", keyword.toString().toLowerCase());
				}
			}		
		}
	}

	private LinkedHashSet<String> getKeywordsFromResources(String KeywordsResourceLocation) {

		LinkedHashSet<String> hashSet = new LinkedHashSet<>();

		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(KeywordsResourceLocation)))) {

			String readline = "";
			while ((readline = bufferedReader.readLine()) != null) {
				hashSet.add(readline);
			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return hashSet;
	}

	public int urlPathDepth(String url, NutchDocument doc) {

		int lastIndex = 0;
		int count = 1;
		// int defaultPathDepth = getConf().getInt(DEFAULTURLPATHDEPTH, 10);
		URL urlForDepth;

		try {
			urlForDepth = new URL(url);
			String path = urlForDepth.getPath();
			if (path != null) {

				path = path.trim();
				if (path.isEmpty() || path.equalsIgnoreCase("/")) {
					// here count is one as for seed page we are considering
					// depth as one
					count = 1;
				} else {
					// here the count is 2 as we are considering urls after seed
					// on depth 2
					count = 2;
					while (lastIndex != -1) {
						lastIndex = path.indexOf("/", lastIndex + 1);
						if (lastIndex != -1) {
							count++;
						}
					}
				}
			}
		} catch (Exception exception) {

		}
		doc.add("depth", count - 1);
		return count - 1;
	}

	// Add time related meta info. Add last-modified if present. Index date as
	// last-modified, or, if that's not present, use fetch time.
	private NutchDocument addTime(NutchDocument doc, ParseData data, String url, CrawlDatum datum) {
		long time = lastTime;
		time = datum.getModifiedTime(); // use value in CrawlDatum
		if (time <= 0) { // if also unset
			time = datum.getFetchTime(); // use time the fetch took place
											// (fetchTime of fetchDatum)
		}
		doc.add("fetchTime", new Date(time));

		String lastModified = data.getMeta(Metadata.LAST_MODIFIED);
		if (lastModified != null) { // try parse last-modified
			time = useNattyParser(lastModified).getTime();
			doc.add("lastModified", new Date(time));
		}

		return doc;
	}

	public NutchDocument addPublishDate(NutchDocument doc, ParseData data, String url, CrawlDatum datum) {
		String publishdate = "";
		LOG.info("Date module indexing started");

		if (data.getMeta("publishdate") != null) {
			publishdate = (data.getMeta("publishdate")).trim();
			LOG.info("Publishdate from getMeta(publishdate) " + publishdate);

		} else if (data.getMeta("metatag.publishdate") != null) {
			publishdate = data.getMeta("metatag.publishdate");
			LOG.info("Publishdate from getMeta(metatag.publishdate) " + publishdate);

		} else {
			publishdate = data.getMeta(Metadata.LAST_MODIFIED);
			LOG.info("Publishdate from Metadata.LAST_MODIFIED " + publishdate);
		}
		LOG.info("Publishdate recieved from meta for url :" + url + " : " + publishdate);
		String formattedDate = "";
		long time = lastTime;

		time = getTimeDirectly(publishdate, url);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		formattedDate = formatter.format(time);

		LOG.info("FINAL TIME  " + time + " AND FORMATTED DATE IS ==> " + formattedDate + " -- for URL: " + url);

		doc.add("articleDate", formattedDate); // schema value articleDate
		LOG.info("Adding in index completed for date");

		return doc;
	}

	private long getTimeFromWebservice(String publishdate, String url) throws IOException {
		long time = -1;

		NormalizeDateLocator service = new NormalizeDateLocator();
		NormalizeDateSoap port;
		String outDateFromWebService = "";
		try {
			port = service.getNormalizeDateSoap();
			outDateFromWebService = port.normalizeDateFunction(publishdate);
			LOG.info("Webservice call success:" + outDateFromWebService);
			Pattern patternDate = Pattern.compile("([0-9]+D_)");
			Pattern patternMonth = Pattern.compile("([0-9]+M_)");
			Pattern patternYear = Pattern.compile("([0-9]+Y_)");

			Matcher matcher = null;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(sdf.parse(lastdate));

			matcher = patternDate.matcher(outDateFromWebService);
			if (matcher.find()) {
				System.out.println("Date found is:" + matcher.group().replaceAll("D_", ""));

				calendar.set(Calendar.DATE, Integer.parseInt(matcher.group().replaceAll("D_", "")));

			}
			matcher = patternMonth.matcher(outDateFromWebService);
			if (matcher.find()) {
				System.out.println("Month found is:" + matcher.group().replaceAll("M_", ""));
				calendar.set(Calendar.MONTH, Integer.parseInt(matcher.group().replaceAll("M_", "")));
			}
			matcher = patternYear.matcher(outDateFromWebService);
			if (matcher.find()) {
				System.out.println("Year found is:" + matcher.group().replaceAll("Y_", ""));
				calendar.set(Calendar.YEAR, Integer.parseInt(matcher.group().replaceAll("Y_", "")));
			}

			Date d = calendar.getTime();
			time = d.getTime();

		} catch (Exception e) {
			BufferedWriter bfw = new BufferedWriter(new FileWriter(new File("/opt/p-akhbar-13oct2017/datesidentified"), true));
			bfw.write(publishdate + "\t" + outDateFromWebService + "\t" + url + "\n");
			bfw.close();
			e.printStackTrace();
		}

		if (time == lastTime) {
			BufferedWriter bfw = new BufferedWriter(new FileWriter(new File("/opt/p-akhbar-13oct2017/datesidentified"), true));
			bfw.write(publishdate + "\t" + outDateFromWebService + "\t" + url + "\n");
			bfw.close();
		}
		return time;
	}

	private long getTimeDirectly(String publishdate, String url) {
		long time = -1;

		System.out.println("url:" + url);
		try {
			time = sdf.parse(publishdate).getTime();
		} catch (Exception e) {
			try {
				LOG.info("Setting the time for " + url + " to default value in indexer!");
				time = lastTime;
				BufferedWriter bfw = new BufferedWriter(new FileWriter(new File("/opt/p-akhbar-13oct2017/DateUnidentified"), true));
				bfw.write(url + "\n");
				bfw.close();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
		}
		return time;
	}

	private Date useNattyParser(String textinput) {
		if (nattyParser == null)
			nattyParser = new NattyParser();
		List<DateGroup> groups = nattyParser.parse(textinput);
		List<Date> dates = new ArrayList<>();
		for (DateGroup group : groups) {
			dates = group.getDates();
			int line = group.getLine();
			int column = group.getPosition();
			String matchingValue = group.getText();
			String syntaxTree = group.getSyntaxTree().toStringTree();
			Map<String, List<ParseLocation>> parseMap = group.getParseLocations();
			boolean isRecurreing = group.isRecurring();
			Date recursUntil = group.getRecursUntil();
			LOG.info("dates:" + dates + "\nline " + line + " and column " + column + "\nmatchingValue:" + matchingValue
					+ "\nsyntaxTree:" + syntaxTree + "\nparseMap:" + parseMap + "\nisRecurreing:" + isRecurreing
					+ "\nrecursUntil:" + recursUntil);
		}
		return dates.get(0);
	}

	public void setConf(Configuration conf) {

		this.conf = conf;
	}

	public Configuration getConf() {
		return this.conf;
	}

}
