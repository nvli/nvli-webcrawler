/**
 * NormalizeDateLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class NormalizeDateLocator extends org.apache.axis.client.Service implements org.tempuri.NormalizeDate {

    public NormalizeDateLocator() {
    }


    public NormalizeDateLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public NormalizeDateLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for NormalizeDateSoap
    private java.lang.String NormalizeDateSoap_address = "http://joel-pc/NormalizeDateService/NormalizeDate.asmx";

    public java.lang.String getNormalizeDateSoapAddress() {
        return NormalizeDateSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String NormalizeDateSoapWSDDServiceName = "NormalizeDateSoap";

    public java.lang.String getNormalizeDateSoapWSDDServiceName() {
        return NormalizeDateSoapWSDDServiceName;
    }

    public void setNormalizeDateSoapWSDDServiceName(java.lang.String name) {
        NormalizeDateSoapWSDDServiceName = name;
    }

    public org.tempuri.NormalizeDateSoap getNormalizeDateSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(NormalizeDateSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getNormalizeDateSoap(endpoint);
    }

    public org.tempuri.NormalizeDateSoap getNormalizeDateSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.NormalizeDateSoapStub _stub = new org.tempuri.NormalizeDateSoapStub(portAddress, this);
            _stub.setPortName(getNormalizeDateSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setNormalizeDateSoapEndpointAddress(java.lang.String address) {
        NormalizeDateSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.tempuri.NormalizeDateSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.NormalizeDateSoapStub _stub = new org.tempuri.NormalizeDateSoapStub(new java.net.URL(NormalizeDateSoap_address), this);
                _stub.setPortName(getNormalizeDateSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("NormalizeDateSoap".equals(inputPortName)) {
            return getNormalizeDateSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "NormalizeDate");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "NormalizeDateSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("NormalizeDateSoap".equals(portName)) {
            setNormalizeDateSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
