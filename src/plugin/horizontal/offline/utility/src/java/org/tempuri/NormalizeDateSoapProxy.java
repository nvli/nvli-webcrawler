package org.tempuri;

public class NormalizeDateSoapProxy implements org.tempuri.NormalizeDateSoap {
  private String _endpoint = null;
  private org.tempuri.NormalizeDateSoap normalizeDateSoap = null;
  
  public NormalizeDateSoapProxy() {
    _initNormalizeDateSoapProxy();
  }
  
  public NormalizeDateSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initNormalizeDateSoapProxy();
  }
  
  private void _initNormalizeDateSoapProxy() {
    try {
      normalizeDateSoap = (new org.tempuri.NormalizeDateLocator()).getNormalizeDateSoap();
      if (normalizeDateSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)normalizeDateSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)normalizeDateSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (normalizeDateSoap != null)
      ((javax.xml.rpc.Stub)normalizeDateSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.NormalizeDateSoap getNormalizeDateSoap() {
    if (normalizeDateSoap == null)
      _initNormalizeDateSoapProxy();
    return normalizeDateSoap;
  }
  
  public java.lang.String normalizeDateFunction(java.lang.String indate) throws java.rmi.RemoteException{
    if (normalizeDateSoap == null)
      _initNormalizeDateSoapProxy();
    return normalizeDateSoap.normalizeDateFunction(indate);
  }
  
  
}