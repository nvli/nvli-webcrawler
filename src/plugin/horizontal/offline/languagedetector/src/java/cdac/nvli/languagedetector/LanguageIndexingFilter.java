/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cdac.nvli.languagedetector;

// Nutch imports
import org.apache.nutch.crawl.CrawlDatum;
import org.apache.nutch.crawl.Inlinks;
import org.apache.nutch.indexer.IndexingFilter;
import org.apache.nutch.indexer.IndexingException;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.hadoop.io.Text;
import org.apache.nutch.parse.Parse;
import org.apache.nutch.metadata.Metadata;
import org.apache.nutch.net.protocols.Response;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

// Hadoop imports
import org.apache.hadoop.conf.Configuration;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

public class LanguageIndexingFilter implements IndexingFilter {

	private Configuration conf;
	private LangDetectException cause = null;
	private static Logger LOG = Logger.getLogger("cdac.nvli.languagedetector.LanguageIndexingFilter");

	public LanguageIndexingFilter() {

	}

	// Inherited JavaDoc
	public NutchDocument filter(NutchDocument doc, Parse parse, Text url, CrawlDatum datum, Inlinks inlinks)
			throws IndexingException {
		if (conf == null) {
			throw new IndexingException("No Initialization, yet!");
		}
		if (cause != null) {
			throw new IndexingException("Initialization Failed.", cause);
		}
		String lang = "";

		// Chech via Language Parser, code added by lovey@aai.cdac-- June
		// 14,2016
		if (lang == null || lang.equals("")) {
			LOG.info("Checking language for url:" + url.toString());
			StringBuilder text = new StringBuilder();
			text.append(parse.getData().getTitle()).append(" ").append(parse.getText());
			try {
				Detector detector = DetectorFactory.create();

				detector.append(text.toString());
				lang = detector.detect();
				LOG.info("Language identified: " + lang);

			} catch (LangDetectException e) {
				throw new IndexingException("Detection failed.", e);
			}
		}
		if (lang.equals("")) {
			// check if LANGUAGE found, possibly put there by HTMLLanguageParser

			lang = parse.getData().getParseMeta().get(Metadata.LANGUAGE);

		}
		// check if HTTP-header tels us the language
		if (lang.equals("")) {
			lang = parse.getData().getContentMeta().get(Response.CONTENT_LANGUAGE);
		}

		if (lang.length() > 2) // only two char codes
			lang = StringUtils.substring(lang, 0, 2);
		if (lang.length() == 0 || lang.equals("")) {

			lang = "unknown";
		}

		doc.add("lang", lang);

		return doc;
	}

	public void setConf(Configuration conf) {
		if (this.conf == null) {
			try {
				LOG.info("Setting conf for language detection!");
				DetectorFactory.loadProfile(conf.get("langdetect.profile.dir"));

				LOG.info("Setting conf for language detection,completed!");
			} catch (LangDetectException e) {
				// afterward throw when filter() is called
				e.printStackTrace();
				cause = e;
			}
		}
		this.conf = conf;
	}

	public Configuration getConf() {
		return this.conf;
	}

	public static void main(String[] args) throws Exception {

		try {
			DetectorFactory.loadProfile(new File(
					"/media/lovey/99d965a4-7ebd-4452-acc2-779ac18042e9/lovey/workspace/NVLIWorkplace/WorkspaceForGit/nvliWebCrawler/resources/profiles"));
		} catch (LangDetectException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			Document doc = Jsoup.parse(
					new URL("http://timesofindia.indiatimes.com/india/rain-clouds-thinning-out-in-west-and-central-india/articleshow/59152218.cms"),
					60000);
			String lang = "";
			LOG.info("Checking language--- Lovey");
			String content = doc.body().text();
			StringBuilder text = new StringBuilder();
			text.append(content);
			try {
				Detector detector = DetectorFactory.create();

				detector.append(text.toString());
				lang = detector.detect();
				LOG.info("Language identified: " + lang);
				if (lang.length() > 2)
					lang = StringUtils.substring(lang, 0, 2);
				LOG.info("Language identified: " + lang);
			} catch (LangDetectException e) {
				throw new Exception("Detection failed.", e);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
