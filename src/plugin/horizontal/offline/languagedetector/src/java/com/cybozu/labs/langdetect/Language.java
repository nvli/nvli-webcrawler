package com.cybozu.labs.langdetect;

import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.util.NutchConfiguration;

/**
 * {@link Language} is to store the detected language.
 * {@link Detector#getProbabilities()} returns an {@link ArrayList} of {@link Language}s.
 *  
 * @see Detector#getProbabilities()
 * @author Nakatani Shuyo
 *
 */
public class Language {
	public static String[] supportedlangs;
    public String lang;
    public double prob;
    private static Configuration conf = null;
    public static final String SUPPORTED_LANGUAGE_NUTCH_SITE = "langdetect.profile.supported";
    private static String SUPPORTED_LANGUAGE_NUTCH_SITE_VALUE;
    static{
    	conf =  NutchConfiguration.create();
    	System.out.println("Conf "+conf.toString());
    	SUPPORTED_LANGUAGE_NUTCH_SITE_VALUE="ar|bn|en|gu|hi|kn|ml|mr|ne|pa|ta|te";
    	/*SUPPORTED_LANGUAGE_NUTCH_SITE_VALUE=conf.get(SUPPORTED_LANGUAGE_NUTCH_SITE);*/
    	System.out.println("Supported languages: "+SUPPORTED_LANGUAGE_NUTCH_SITE_VALUE);
    	supportedlangs=SUPPORTED_LANGUAGE_NUTCH_SITE_VALUE.split("|");
    }
    public Language(String lang, double prob) {
        this.lang = lang;
        this.prob = prob;
        
        
        
    }
    public String toString() {
        if (lang==null) return "";
        return lang + ":" + prob;
    }
    public static void main(String[] args)
    {
    	
    }
}
