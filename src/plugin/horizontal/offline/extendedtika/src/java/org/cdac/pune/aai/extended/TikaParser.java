/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.cdac.pune.aai.extended;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.apache.nutch.util.*;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.html.dom.HTMLDocumentImpl;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.parse.HTMLMetaTags;
import org.apache.nutch.parse.HtmlParseFilters;
import org.apache.nutch.parse.Outlink;
import org.apache.nutch.parse.OutlinkExtractor;
import org.apache.nutch.parse.Parse;
import org.apache.nutch.parse.ParseData;
import org.apache.nutch.parse.ParseImpl;
import org.apache.nutch.parse.ParseResult;
import org.apache.nutch.parse.ParseStatus;
import org.cdac.pune.aai.extended.DOMBuilder;
import org.cdac.pune.aai.extended.DOMContentUtils;
import org.cdac.pune.aai.extended.HTMLMetaProcessor;
import org.apache.nutch.protocol.Content;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.html.HtmlMapper;
import org.apache.tika.sax.XHTMLContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DocumentFragment;

import com.joestelmach.natty.DateGroup;
import com.joestelmach.natty.NattyParser;
import com.joestelmach.natty.ParseLocation;

import org.jsoup.nodes.Document;

/**
 * @author pragya
 * @version extended version
 * Wrapper for Tika parsers. Mimics the HTMLParser but using the XHTML
 * representation returned by Tika as SAX events
 ***/

public class TikaParser implements org.apache.nutch.parse.Parser {

  public static final Logger LOG = LoggerFactory.getLogger(TikaParser.class);

  private Configuration conf;
  private TikaConfig tikaConfig = null;
  private DOMContentUtils utils;
  private HtmlParseFilters htmlParseFilters;
  private String cachingPolicy;
  private HtmlMapper HTMLMapper;
  private boolean upperCaseElementNames = true;
  public static final String URL_SPECIFIC_PARAMS = "urlparams.dir.file";
  public static Map<String, URLParams> urlParamsMap;
  public static Map<String, ArrayList<String>> CleanedUrlMap;
  private static NattyParser nattyParser;
  private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
  
  @SuppressWarnings("deprecation")
  public ParseResult getParse(Content content) {
	  System.out.println("========in EXTENDED TIKA Parse AT 94======");
	  LOG.info("at Extended Parser 94");
    String mimeType = content.getContentType();
    String urlParamsResource = conf.get(URL_SPECIFIC_PARAMS);
    URL base;
    try {
      base = new URL(content.getBaseUrl());
    } catch (MalformedURLException e) {
      return new ParseStatus(e)
          .getEmptyParseResult(content.getUrl(), getConf());
    }

    // get the right parser using the mime type as a clue
    Parser parser = tikaConfig.getParser(MediaType.parse(mimeType));
    byte[] raw = content.getContent();

    if (parser == null) {
      String message = "Can't retrieve Tika parser for mime-type " + mimeType;
      LOG.error(message);
      return new ParseStatus(ParseStatus.FAILED, message).getEmptyParseResult(
          content.getUrl(), getConf());
    }

    LOG.debug("Using Tika parser " + parser.getClass().getName()
        + " for mime-type " + mimeType);

    Metadata tikamd = new Metadata();

    
    CleanedUrlMap=new HashMap<>();
	urlParamsMap = getURLParams(urlParamsResource);
    
	
	
    HTMLDocumentImpl doc = new HTMLDocumentImpl();
    doc.setErrorChecking(false);
    DocumentFragment root = doc.createDocumentFragment();
    DOMBuilder domhandler = new DOMBuilder(doc, root);
    domhandler.setUpperCaseElementNames(upperCaseElementNames);
    domhandler.setDefaultNamespaceURI(XHTMLContentHandler.XHTML);

    ParseContext context = new ParseContext();
    if (HTMLMapper != null)
      context.set(HtmlMapper.class, HTMLMapper);
    tikamd.set(Metadata.CONTENT_TYPE, mimeType);
    try {
      parser.parse(new ByteArrayInputStream(raw), domhandler, tikamd, context);
    } catch (Exception e) {
      LOG.error("Error parsing " + content.getUrl(), e);
      return new ParseStatus(ParseStatus.FAILED, e.getMessage())
          .getEmptyParseResult(content.getUrl(), getConf());
    }

    HTMLMetaTags metaTags = new HTMLMetaTags();
    String text = "";
    String title = "";
    Outlink[] outlinks = new Outlink[0];
    org.apache.nutch.metadata.Metadata nutchMetadata = new org.apache.nutch.metadata.Metadata();

    // we have converted the sax events generated by Tika into a DOM object
    // so we can now use the usual HTML resources from Nutch
    // get meta directives
    HTMLMetaProcessor.getMetaTags(metaTags, root, base);
    if (LOG.isTraceEnabled()) {
      LOG.trace("Meta tags for " + base + ": " + metaTags.toString());
    }

    // check meta directives
    if (!metaTags.getNoIndex()) { // okay to index
      StringBuffer sb = new StringBuffer();
      if (LOG.isTraceEnabled()) {
        LOG.trace("Getting text...");
      }
      utils.getText(sb, root); // extract text
      text = sb.toString();
      sb.setLength(0);
      if (LOG.isTraceEnabled()) {
        LOG.trace("Getting title...");
      }
      utils.getTitle(sb, root); // extract title
      title = sb.toString().trim();
    }

    if (!metaTags.getNoFollow()) { // okay to follow links
      ArrayList<Outlink> l = new ArrayList<Outlink>(); // extract outlinks
      URL baseTag = utils.getBase(root);
      if (LOG.isTraceEnabled()) {
        LOG.trace("Getting links...");
      }
      utils.getOutlinks(baseTag != null ? baseTag : base, l, root);
      outlinks = l.toArray(new Outlink[l.size()]);
      if (LOG.isTraceEnabled()) {
        LOG.trace("found " + outlinks.length + " outlinks in "
            + content.getUrl());
      }
    }

    // populate Nutch metadata with Tika metadata
    String[] TikaMDNames = tikamd.names();
    for (String tikaMDName : TikaMDNames) {
      if (tikaMDName.equalsIgnoreCase(Metadata.TITLE))
        continue;
      String[] values = tikamd.getValues(tikaMDName);
      for (String v : values)
        nutchMetadata.add(tikaMDName, v);
    }

    // no outlinks? try OutlinkExtractor e.g works for mime types where no
    // explicit markup for anchors

    if (outlinks.length == 0) {
      outlinks = OutlinkExtractor.getOutlinks(text, getConf());
    }

    try {
    	nutchMetadata=addFieldsToMetadata(urlParamsMap,content.getUrl(), text,nutchMetadata);
	} catch (URISyntaxException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    ParseStatus status = new ParseStatus(ParseStatus.SUCCESS);
    if (metaTags.getRefresh()) {
      status.setMinorCode(ParseStatus.SUCCESS_REDIRECT);
      status.setArgs(new String[] { metaTags.getRefreshHref().toString(),
          Integer.toString(metaTags.getRefreshTime()) });
    }
    ParseData parseData = new ParseData(status, title, outlinks,
        content.getMetadata(), nutchMetadata);
    ParseResult parseResult = ParseResult.createParseResult(content.getUrl(),
        new ParseImpl(text, parseData));

    // run filters on parse
    ParseResult filteredParse = this.htmlParseFilters.filter(content,
        parseResult, metaTags, root);
    if (metaTags.getNoCache()) { // not okay to cache
      for (Map.Entry<org.apache.hadoop.io.Text, Parse> entry : filteredParse)
        entry.getValue().getData().getParseMeta()
            .set(Nutch.CACHING_FORBIDDEN_KEY, cachingPolicy);
    }
    return filteredParse;
  }
  
  public org.apache.nutch.metadata.Metadata addFieldsToMetadata(Map<String, URLParams> urlParamsMap, String incomingUrl,String text, org.apache.nutch.metadata.Metadata metadata) throws URISyntaxException, IOException
	{
		String host = "";
		String hostName="";
		String idCode="";
		String resourceType="";
		String identifier="";
		String dateClass="";
		String publishedDate="";
		URLParams up= new URLParams();
		URL u_modified = IdUtils.getUrl(incomingUrl);
		String cleanedurl = IdUtils.cleanUrls(u_modified);
		
				
		try {
			host = new URL(incomingUrl).getHost();
			System.out.println("========HOST AT 614======" + host);
			
			/*for(String url:CleanedUrlMap.keySet())
			{
				//System.out.println("========URL AT 618======" + url+ "cleaned url is---" +CleanedUrlMap.get(url));
				
				if(host.contains(CleanedUrlMap.get(url))) 
				{
					//System.out.println("========URL AT 622 HOST FOUND ======" + urlParamsMap.get(url));
					up = urlParamsMap.get(url);
				}
				
				
			}*/
			
			for(String url:CleanedUrlMap.keySet())
			{
				//System.out.println("========URL AT 618======" + url+ "cleaned url is---" +CleanedUrlMap.get(url));
				for(String cleanedurls:CleanedUrlMap.get(url))
				{
					//System.out.println("========URL AT 619swathi======" + url+ "cleaned url is---" +cleanedurls);
					if(incomingUrl.replaceAll("http://", "").replaceAll("https://", "").replaceAll("www", "").replaceAll("/", "").contains(cleanedurls))
					{
						System.out.println("========URL AT 622 HOST FOUND ======" + urlParamsMap.get(cleanedurls));
						up = urlParamsMap.get(cleanedurls);
					}
				}
				
				
			}
			
			
			System.out.println("========URLParams OBJECT AT 610======" + up.toString());
			
			
			
			
			if (up != null) {
				
					idCode=up.getIdcode();
					System.out.println("========URLParams OBJECT AT 286======" + up.toString());
					
					resourceType=up.getResourceType();
					
					identifier=resourceType+"_" + idCode;
					
					hostName=up.getNewsSource();
					
					
					if(text!=null| !text.isEmpty())
						publishedDate = sdf.format(useNattyParser(text));
					else
					{
						publishedDate = sdf.format(new Date());
					}
					
				}
				
			identifier=identifier+"_"+sdf.format(new Date());
			//Check whether the identifed publish date is after TODAY's date
			publishedDate = checkDateModule(publishedDate);
			
			System.out.println("========HOST AT 608======" + host);
			System.out.println("========IDCODE AT 608======" + idCode);
			System.out.println("========HOSTNAME AT 608======" + hostName);
			System.out.println("========RESOURCETYPE AT 608======" + resourceType);
			System.out.println("========IDENTIFIER AT 608======" + identifier);
			System.out.println("========PUBLISHDATE AT 608======" + publishedDate);
			
			
			
			metadata.add("idcode", idCode);
			metadata.add("hostname", hostName);
			metadata.add("resourceType", resourceType);
			metadata.add("baseUrl", host);
			metadata.add("identifier", identifier); 
			metadata.add("publishdate", publishedDate);
			
			return metadata;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			IdUtils.getIdentifier(cleanedurl, incomingUrl, getConf());
			
			idCode=IdUtils.IDCODE;
			if(idCode==null)
				idCode="NOTFND";
			
			
			
			identifier=IdUtils.IDENTIFIER;
			
			resourceType=IdUtils.RESOURCETYPE;
			
			IdUtils.getHostname(resourceType, u_modified, getConf());
			
			hostName=IdUtils.IDENTIFIED_HOSTNAME;
			
			if(hostName==null)
				hostName=host;
			
			if(text!=null| !text.isEmpty())
				publishedDate = sdf.format(useNattyParser(text));
			else
			{
				publishedDate = sdf.format(new Date());
			}
			identifier=identifier+"_"+sdf.format(new Date());
			//Check whether the identifed publish date is after TODAY's date
			publishedDate = checkDateModule(publishedDate);
			
			System.out.println("========HOST AT 685======" + host);
			System.out.println("========IDCODE AT 686======" + idCode);
			System.out.println("========HOSTNAME AT 687======" + hostName);
			System.out.println("========RESOURCETYPE AT 688======" + resourceType);
			System.out.println("========IDENTIFIER AT 689======" + identifier);
			System.out.println("========PUBLISHDATE AT 690======" + publishedDate);
			
			
			
			metadata.add("idcode", idCode);
			metadata.add("hostname", hostName);
			metadata.add("resourceType", resourceType);
			metadata.add("baseUrl", host);
			metadata.add("identifier", identifier); 
			metadata.add("publishdate", publishedDate);
			
			
			return metadata;
		}
		
	}
  private Date useNattyParser(String textinput) {
		if (nattyParser == null)
			nattyParser = new NattyParser(TimeZone.getTimeZone("Asia/Kolkata"));
		List<DateGroup> groups = nattyParser.parse(textinput);
		List<Date> dates = new ArrayList<>();
		for (DateGroup group : groups) {
			dates = group.getDates();
			int line = group.getLine();
			int column = group.getPosition();
			String matchingValue = group.getText();
			String syntaxTree = group.getSyntaxTree().toStringTree();
			Map<String, List<ParseLocation>> parseMap = group.getParseLocations();
			boolean isRecurreing = group.isRecurring();
			Date recursUntil = group.getRecursUntil();

		}
		
		if(!dates.isEmpty())
				return dates.get(0);
		else
			return new Date();
	}
  /* =============code added by pragya to read XSL file============== */
	private Map<String, URLParams> getURLParams(String filePath) {
		Map<String, URLParams> urlParams = new HashMap<String, URLParams>();
		System.out.println("IN GET URLParams FUNCTION"); 
		
		FileInputStream input_document;
		try {
			input_document = new FileInputStream(new File(filePath));
			int count = 1;
			int celCnt;
			// Read XLS document - Office 97 -2003 format
			HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document);
			HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
			//System.out.println("worksheet is " + my_worksheet.getSheetName());

			int rowcount = my_worksheet.getPhysicalNumberOfRows();
			//System.out.println("number of rows is*****" + rowcount);
			Iterator<Row> rowIterator = my_worksheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				// Read Rows from Excel document
				/*
				 * if( row == null ){ System.out.println("rows is Empty*****"+rowcount);
				 * break; } else
				 */
				count += 1;
				//System.out.println("current count is*****" + count);
				if (count <= rowcount) {
					Row row = rowIterator.next();

					Iterator<Cell> cellIterator = row.cellIterator();

					try {
						String url = row.getCell(0).getStringCellValue();
						String cleanedUrl=url.replaceAll("http://", "").replaceAll("https://", "").replaceAll("www", "").replaceAll("/", "");
						
						String hubEnds = row.getCell(1).getStringCellValue();
						String articleEnds = row.getCell(2).getStringCellValue();
						String divClass = row.getCell(3).getStringCellValue();
						String titleClass = row.getCell(4).getStringCellValue();
						String depth = "2";/*
											 * String.valueOf(row.getCell(5).
											 * getNumericCellValue());* error
											 * occuring
											 */
						String newsSource = row.getCell(6).getStringCellValue();
						String leadingPatt = row.getCell(7).getStringCellValue();
						String trailingPatt = row.getCell(8).getStringCellValue();
						String classForDate = row.getCell(9).getStringCellValue();
						String dateLeadingPattern = row.getCell(10).getStringCellValue();

						String dateTrailingPattern = row.getCell(11).getStringCellValue();
						String idcode=row.getCell(12).getStringCellValue();
						String resourceType=row.getCell(13).getStringCellValue();
						//System.out.println("");
						URI u = new URI(url);
						String host = "";
						try {
							host = u.getHost();
						} catch (Exception e) {
							e.printStackTrace();
							continue;
						}
						/* String host = new URL(new URI(url)).getHost(); */

						/*System.out.println(url + "--" + hubEnds + "==" + articleEnds +
						 "==" + divClass + "==" + titleClass + "==" + depth +
						 "==" + newsSource + "==" + newsSource + "==" +
						 leadingPatt + "==" + trailingPatt + "==" + classForDate +
						 "==" + idcode);*/
						//CleanedUrlMap.put(host,cleanedUrl);
						if(CleanedUrlMap.containsKey(host)){
							CleanedUrlMap.get(host).add(cleanedUrl);
						}else{
							ArrayList<String> newList = new ArrayList<String>();;
							newList.add(cleanedUrl);
							CleanedUrlMap.put(host, newList);
						}
						urlParams.put(cleanedUrl, new URLParams(url, hubEnds, articleEnds, divClass, titleClass, depth,
								newsSource, leadingPatt, trailingPatt, classForDate, dateLeadingPattern,dateTrailingPattern,idcode,resourceType,cleanedUrl));
					} catch (Exception e) {
						System.out.println("Exception occrred in getUrlParams");
						e.printStackTrace();
						continue;
					}
					
				}
			}
			input_document.close(); // Close the XLS file opened for printing
		} catch (Exception e) {
			e.printStackTrace();
		}

		return urlParams;
	}
	public String checkDateModule(String identifieddate) {

		Date currentdate = new Date();
		try {
			if (currentdate.before(sdf.parse(identifieddate))) {
				return sdf.format(currentdate);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return identifieddate;

	}
  public void setConf(Configuration conf) {
    this.conf = conf;
    this.tikaConfig = null;

    // do we want a custom Tika configuration file
    // deprecated since Tika 0.7 which is based on
    // a service provider based configuration
    String customConfFile = conf.get("tika.config.file");
    if (customConfFile != null) {
      try {
        // see if a Tika config file can be found in the job file
        URL customTikaConfig = conf.getResource(customConfFile);
        if (customTikaConfig != null)
          tikaConfig = new TikaConfig(customTikaConfig);
      } catch (Exception e1) {
        String message = "Problem loading custom Tika configuration from "
            + customConfFile;
        LOG.error(message, e1);
      }
    } else {
      try {
        tikaConfig = new TikaConfig(this.getClass().getClassLoader());
      } catch (Exception e2) {
        String message = "Problem loading default Tika configuration";
        LOG.error(message, e2);
      }
    }

    // use a custom htmlmapper
    String htmlmapperClassName = conf.get("tika.htmlmapper.classname");
    if (StringUtils.isNotBlank(htmlmapperClassName)) {
      try {
        Class HTMLMapperClass = Class.forName(htmlmapperClassName);
        boolean interfaceOK = HtmlMapper.class
            .isAssignableFrom(HTMLMapperClass);
        if (!interfaceOK) {
          throw new RuntimeException("Class " + htmlmapperClassName
              + " does not implement HtmlMapper");
        }
        HTMLMapper = (HtmlMapper) HTMLMapperClass.newInstance();
      } catch (Exception e) {
        LOG.error("Can't generate instance for class " + htmlmapperClassName);
        throw new RuntimeException("Can't generate instance for class "
            + htmlmapperClassName);
      }
    }

    this.htmlParseFilters = new HtmlParseFilters(getConf());
    this.utils = new DOMContentUtils(conf);
    this.cachingPolicy = getConf().get("parser.caching.forbidden.policy",
        Nutch.CACHING_FORBIDDEN_CONTENT);
    this.upperCaseElementNames = getConf().getBoolean(
        "tika.uppercase.element.names", true);
  }

  public Configuration getConf() {
    return this.conf;
  }

}
