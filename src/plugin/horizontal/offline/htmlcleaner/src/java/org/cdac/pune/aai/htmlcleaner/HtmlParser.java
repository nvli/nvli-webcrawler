/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.cdac.pune.aai.htmlcleaner;

import java.util.*;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.net.URL;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.io.*;
import java.util.regex.*;

import org.cyberneko.html.parsers.*;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.DataNode;
import org.jsoup.select.Elements;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.joestelmach.natty.*;
import com.sun.research.ws.wadl.ResourceType;

import de.jetwick.snacktory.HtmlFetcher;
import de.jetwick.snacktory.JResult;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import org.w3c.dom.*;
import org.apache.html.dom.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.nutch.metadata.Metadata;
import org.apache.nutch.metadata.Nutch;
import org.apache.nutch.protocol.Content;
import org.apache.hadoop.conf.*;
import org.apache.nutch.parse.*;

import org.apache.nutch.util.*;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;


import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.extractors.CommonExtractors;
import de.l3s.boilerpipe.sax.BoilerpipeSAXInput;
import de.l3s.boilerpipe.sax.HTMLDocument;
import de.l3s.boilerpipe.sax.HTMLFetcher;

public class HtmlParser implements Parser {
	public static final Logger LOG = LoggerFactory.getLogger("org.apache.nutch.parse.html");
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private static String lastdate = "0001-01-00T00:00:01Z";
	private static NattyParser nattyParser;

	// I used 1000 bytes at first, but found that some documents have
	// meta tag well past the first 1000 bytes.
	// (e.g. http://cn.promo.yahoo.com/customcare/music.html)
	private static final int CHUNK_SIZE = 2000;

	// NUTCH-1006 Meta equiv with single quotes not accepted
	private static Pattern metaPattern = Pattern.compile("<meta\\s+([^>]*http-equiv=(\"|')?content-type(\"|')?[^>]*)>",
			Pattern.CASE_INSENSITIVE);
	private static Pattern charsetPattern = Pattern.compile("charset=\\s*([a-z][_\\-0-9a-z]*)",
			Pattern.CASE_INSENSITIVE);
	private static Pattern charsetPatternHTML5 = Pattern
			.compile("<meta\\s+charset\\s*=\\s*[\"']?([a-z][_\\-0-9a-z]*)[^>]*>", Pattern.CASE_INSENSITIVE);

	private String parserImpl;
        private ArrayList<ArrayList<String>> dates = new ArrayList<ArrayList<String>>();


	private Configuration conf;
	
	
	public static final String DATE_PATTERN = "date_formatter_pattern";
	private HtmlParseFilters htmlParseFilters;
	private String defaultCharEncoding;
	private String cachingPolicy;
	public static final String URL_SPECIFIC_PARAMS = "urlparams.dir.file";
	public static final String DATE_FORMATS = "date.formats.file";
	public static final String NER_PHRASES_FILE = "phraseDictionary.dir.file";
	public static final String GLOBAL_HUBS_FILE = "globalhubslist.dir.file";
	public static final String CRAWL_HUBS_FILE = "crawlhubslist.dir.file";

	public static Map<String, URLParams> urlParamsMap;
	public static LinkedHashSet<String> NERPhrasesListFromFile;
	public static Map<String,ArrayList<String>> CleanedUrlMap;
	public ArrayList<String> PubDate = new ArrayList<String>();
	public Boolean PubDatefetched = false;
	String GlobalHubsFile;
	String CrawlHubsFile;
	private static String sniffCharacterEncoding(byte[] content) {
		int length = content.length < CHUNK_SIZE ? content.length : CHUNK_SIZE;

		// We don't care about non-ASCII parts so that it's sufficient
		// to just inflate each byte to a 16-bit value by padding.
		// For instance, the sequence {0x41, 0x82, 0xb7} will be turned into
		// {U+0041, U+0082, U+00B7}.
		String str = "";
		try {
			str = new String(content, 0, length, Charset.forName("ASCII").toString());
		} catch (UnsupportedEncodingException e) {
			// code should never come here, but just in case...
			return null;
		}

		Matcher metaMatcher = metaPattern.matcher(str);
		String encoding = null;
		if (metaMatcher.find()) {
			Matcher charsetMatcher = charsetPattern.matcher(metaMatcher.group(1));
			if (charsetMatcher.find())
				encoding = new String(charsetMatcher.group(1));
		}

		return encoding;
	}

	public String dateFormatter(String rawdate){
		for(ArrayList<String> row:dates){
			for(String value : row){
				rawdate = rawdate.replace(value,row.get(0));
			}
		}
		return 	rawdate;
	}

	public ParseResult getParse(Content content) {
		HTMLMetaTags metaTags = new HTMLMetaTags();
		String datePattern = conf.get(DATE_PATTERN);
		String urlParamsResource = conf.get(URL_SPECIFIC_PARAMS);
		String dateResource = conf.get(DATE_FORMATS);
		String NERPhrasesDictionaryFile = conf.get(NER_PHRASES_FILE);
		GlobalHubsFile = conf.get(GLOBAL_HUBS_FILE);
		CrawlHubsFile = conf.get(CRAWL_HUBS_FILE);
		NERPhrasesListFromFile = new LinkedHashSet<>();
		Metadata metadata = new Metadata();
		URL base;
		try {
			base = new URL(content.getBaseUrl());
		} catch (MalformedURLException e) {
			return new ParseStatus(e).getEmptyParseResult(content.getUrl(), getConf());
		}

		String text = "";
		String title = "";
		String publishedDate = "";

		Outlink[] outlinks = new Outlink[0];
		

		// parse the content
		Document doc = new Document(content.getBaseUrl());
		String encoding="";
		try {
			byte[] contentInOctets = content.getContent();
			InputSource input = new InputSource(new ByteArrayInputStream(contentInOctets));

			EncodingDetector detector = new EncodingDetector(conf);
			detector.autoDetectClues(content, true);
			detector.addClue(sniffCharacterEncoding(contentInOctets), "sniffed");
			encoding = detector.guessEncoding(content, defaultCharEncoding);

			
			input.setEncoding(encoding);
			if (LOG.isTraceEnabled()) {
				LOG.trace("Parsing via JSOUP...");
			}
			doc = parseJsoup(content);
		} catch (IOException e) {
			return new ParseStatus(e).getEmptyParseResult(content.getUrl(), getConf());
		} catch (DOMException e) {
			return new ParseStatus(e).getEmptyParseResult(content.getUrl(), getConf());

		} catch (Exception e) {
			LOG.error("Error: ", e);
			return new ParseStatus(e).getEmptyParseResult(content.getUrl(), getConf());
		}

		/* Add processed additional content into metadata here */

		// ====Read XLS file -- url - pattern match and create a map -- added by
		// Pragya===
		urlParamsMap = new HashMap<>();
		CleanedUrlMap=new HashMap<>();
		urlParamsMap = getURLParams(urlParamsResource);
		getDateParams(dateResource);
		
		Elements metaTags1 = doc.getElementsByTag("meta");
		
		
		for (Element metaTag : metaTags1) {
			for( Attribute attribute : metaTag.attributes() )
    		{
				if(!attribute.getKey().toLowerCase().equals("content"))
				{
		    		if((attribute.getValue().toLowerCase().contains("date") || attribute.getValue().toLowerCase().contains("time")) && (!attribute.getValue().toLowerCase().contains("validate")) && (!attribute.getValue().toLowerCase().contains("times of india")) && !content.getBaseUrl().contains("http://www.dainiksaveratimes.com/"))
		    		{
						PubDate.add((String)metaTag.attr("content"));
						PubDatefetched = true;
		    		}
				}
    		}
		}
		
		if(!PubDatefetched && !content.getBaseUrl().contains("http://www.dainiksaveratimes.com/"))
		{
			Elements scriptElements = doc.getElementsByTag("script");
			////System.out.println("\n\n\n");
				//		//System.out.println(scriptElements);
	 		for (Element element :scriptElements ){                
		    	Pattern p = Pattern.compile("\"datePublished\"(.*):(.*)\"(.+?)\""); // Regex for the value of the key
				Matcher m = p.matcher(element.html()); // you have to use html here and NOT text! Text will drop the 'key' part
				while( m.find() )
				{
					PubDate.add((String) m.group());
					PubDatefetched = true;
					//System.out.println(m.group()); // the whole key ('key = value')
					//System.out.println(m.group(1)); // value only
				}
	  		}
			
			
		}

		
		// get processed article title
		title = getProcessedTitle(urlParamsMap, content.getUrl(), doc);
		// get processed article content
		text = getProcessedContent(urlParamsMap, content.getUrl(), doc);


		System.out.println("\n\n\nTitle   : " + title +"\n");

		System.out.println("\n\nContent    :" + text + "\n");
		//System.out.println("\n\n\nmetaTags in the url are \n\n\n");
		//System.out.println(metaTags);

		/* set metadata map here */
		if (!metaTags.getNoFollow()) { // okay to follow links
			ArrayList<Outlink> l = new ArrayList<Outlink>(); // extract outlinks
			Elements nextLinks = doc.select("a[href]");
			if (LOG.isTraceEnabled()) {
				LOG.trace("Getting links...");
			}
			for (org.jsoup.nodes.Element nextlink : nextLinks) // Iterate over
																// all Links
			{
				final String href = nextlink.absUrl("href");
				if (!href.trim().equalsIgnoreCase(content.getUrl()))
					try {
						l.add(new Outlink(href, ""));
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

			}
			outlinks = l.toArray(new Outlink[l.size()]);
			////System.out.println("found " + outlinks.length + " outlinks in " + content.getUrl());
			if (LOG.isTraceEnabled()) {
				LOG.trace("found " + outlinks.length + " outlinks in " + content.getUrl());
			}
		}
		
		/*add idcode, resourcetype, hostname and date to mertadata*/
		try {
			metadata=addFieldsToMetadata(urlParamsMap,content.getUrl(), doc,metadata);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		metadata.set(Metadata.ORIGINAL_CHAR_ENCODING, encoding);
		metadata.set(Metadata.CHAR_ENCODING_FOR_CONVERSION, encoding);
		ParseStatus status = new ParseStatus(ParseStatus.SUCCESS);
		if (metaTags.getRefresh()) {
			status.setMinorCode(ParseStatus.SUCCESS_REDIRECT);
			status.setArgs(
					new String[] { metaTags.getRefreshHref().toString(), Integer.toString(metaTags.getRefreshTime()) });
		}
		ParseData parseData = new ParseData(status, title, outlinks, content.getMetadata(), metadata);
	
		ParseResult parseResult = ParseResult.createParseResult(content.getUrl(), new ParseImpl(text, parseData));

		return parseResult;
	}


	private Document parseJsoup(Content c) throws Exception {
		Document jsoupdoc = Jsoup.parse(new URL(c.getUrl()), 60000);

		return jsoupdoc;
	}

	public static void main(String[] args) throws Exception {
		// LOG.setLevel(Level.FINE);
		String name = args[0];
		String url = "file:" + name;
		File file = new File(name);
		byte[] bytes = new byte[(int) file.length()];
		DataInputStream in = new DataInputStream(new FileInputStream(file));
		in.readFully(bytes);
		Configuration conf = NutchConfiguration.create();
		HtmlParser parser = new HtmlParser();
		parser.setConf(conf);
		Parse parse = parser.getParse(new Content(url, url, bytes, "text/html", new Metadata(), conf)).get(url);

	}

	public void setConf(Configuration conf) {
		this.conf = conf;
		this.htmlParseFilters = new HtmlParseFilters(getConf());
		this.parserImpl = getConf().get("parser.html.impl", "neko");
		this.defaultCharEncoding = getConf().get("parser.character.encoding.default", "windows-1252");

		this.cachingPolicy = getConf().get("parser.caching.forbidden.policy", Nutch.CACHING_FORBIDDEN_CONTENT);
	}

	public Configuration getConf() {
		return this.conf;
	}

	public static void main() {
		String url = "";
		Document document = Jsoup.parse(url);
		//System.out.println("Content is :\n" + document.body().text());

	}

	/* =============code added by pragya to read XSL file============== */
	private Map<String, URLParams> getURLParams(String filePath) {
		Map<String, URLParams> urlParams = new HashMap<String, URLParams>();
		//System.out.println("IN GET URLParams FUNCTION"); 
		
		InputStream input_document;
		try {
			System.out.println(filePath);
			input_document = new FileInputStream(new File(filePath));
			int count = 1;
			int celCnt;
			//System.out.println("Inside excel get data    " + input_document);
			// Read XLS document - Office 97 -2003 format
			HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document);
			//System.out.println("loaded workbook");
			HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
			//System.out.println("worksheet is " + my_worksheet.getSheetName());
			//System.out.println("loadedworksheet");
			int rowcount = my_worksheet.getPhysicalNumberOfRows();
			//System.out.println("number of rows is*****" + rowcount);
			Iterator<Row> rowIterator = my_worksheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				// Read Rows from Excel document
				/*
				 * if( row == null ){ System.out.println("rows is Empty*****"+rowcount);
				 * break; } else
				 */
				count += 1;
				//System.out.println("current count is*****" + count);
				if (count <= rowcount) {
					Row row = rowIterator.next();

					Iterator<Cell> cellIterator = row.cellIterator();

					try {
						String url = row.getCell(0).getStringCellValue();
						String cleanedUrl=url.replaceAll("http://", "").replaceAll("https://", "").replaceAll("www.", "").replaceAll("www", "").replaceAll("/", "");
						//System.out.println(url+"       URL FROM EXCEL SHEET");
						//System.out.println(cleanedUrl+"       URL AFTER CLEANING");
						
						String hubEnds = row.getCell(1).getStringCellValue();
						String articleEnds = row.getCell(2).getStringCellValue();
						String divClass = row.getCell(3).getStringCellValue();
						String titleClass = row.getCell(4).getStringCellValue();
						String depth = "2";/*
											 * String.valueOf(row.getCell(5).
											 * getNumericCellValue());* error
											 * occuring
											 */
						String newsSource = row.getCell(6).getStringCellValue();
						String leadingPatt = row.getCell(7).getStringCellValue();
						String trailingPatt = row.getCell(8).getStringCellValue();
						String classForDate = row.getCell(9).getStringCellValue();
						String dateLeadingPattern = row.getCell(10).getStringCellValue();

						String dateTrailingPattern = row.getCell(11).getStringCellValue();
						String idcode=row.getCell(12).getStringCellValue();
						
						String resourceType=row.getCell(13).getStringCellValue();
						String unwantedRegex=row.getCell(14).getStringCellValue();
						String boilerpipe=row.getCell(15).getStringCellValue();
						String titleRegex=row.getCell(16).getStringCellValue();
						
						//System.out.println("");
						URI u = new URI(url);
						String host = "";
						try {
							host = u.getHost();
							//System.out.println("\n\n\n"+host+"\n\n\n");
						} catch (Exception e) {
							e.printStackTrace();
							continue;
						}

						if(CleanedUrlMap.containsKey(host)){
							CleanedUrlMap.get(host).add(cleanedUrl);
						}else{
							ArrayList<String> newList = new ArrayList<String>();;
							newList.add(cleanedUrl);
							CleanedUrlMap.put(host, newList);
						}
						urlParams.put(cleanedUrl, new URLParams(url, hubEnds, articleEnds, divClass, titleClass, depth,newsSource, leadingPatt, trailingPatt, classForDate, dateLeadingPattern,dateTrailingPattern,idcode,resourceType,cleanedUrl,unwantedRegex,boilerpipe,titleRegex));
					} catch (Exception e) {
						//System.out.println("Exception occrred in getUrlParams");
						e.printStackTrace();
						continue;
					}
					
				}
			}
			input_document.close(); // Close the XLS file opened for printing
		} catch (Exception e) {
			System.out.println(e);
		}

		return urlParams;
	}


	private void getDateParams(String filePath) {
		
		//System.out.println("IN GET URLParams FUNCTION"); 
		
		FileInputStream input_document;
		try {
			input_document = new FileInputStream(new File(filePath));
			int count = 1;
			int celCnt;
			// Read XLS document - Office 97 -2003 format
			HSSFWorkbook my_xls_workbook = new HSSFWorkbook(input_document);
			HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
			//System.out.println("worksheet is " + my_worksheet.getSheetName());

			int rowcount = my_worksheet.getPhysicalNumberOfRows();
			//System.out.println("number of rows is*****" + rowcount);
			Iterator<Row> rowIterator = my_worksheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				// Read Rows from Excel document
				/*
				 * if( row == null ){ System.out.println("rows is Empty*****"+rowcount);
				 * break; } else
				 */
				count += 1;
				//System.out.println("current count is*****" + count);
				if (count <= rowcount) {
					Row row = rowIterator.next();

					Iterator<Cell> cellIterator = row.cellIterator();
					ArrayList<String> dates_row = new ArrayList<>();
					try {
						for(int i = 0;i<12;i++)
						{
							dates_row.add(row.getCell(i).getStringCellValue());
						}
					dates.add(dates_row);
					} catch (Exception e) {
						//System.out.println("Exception occrred in getUrlParams");
						e.printStackTrace();
						continue;
					}
					
				}
			}
			input_document.close(); // Close the XLS file opened for printing
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/*
	 * by pragya
	 * 
	 * @params Map of specifice site parameters
	 * 
	 * @params incomingUrl
	 * 
	 * @params document
	 * 
	 */
	public String getProcessedContent(Map<String, URLParams> urlParamsMap, String incomingUrl, Document doc) {

		// process content for url specific leading and trailing patterns
		String host = "";
		String content = "";
		try {

			host = new URL(incomingUrl).getHost();
			ArrayList<URLParams> uparams = new ArrayList<URLParams>();
			//System.out.println("\n\n incming url" + incomingUrl);
			for(String url:CleanedUrlMap.keySet())
			{
				//System.out.println("========URL AT 618======" + url+ "cleaned url is---" +CleanedUrlMap.get(url));
				for(String cleanedurls:CleanedUrlMap.get(url))
				{
					//System.out.println("========URL AT 619swathi======" + url+ "cleaned url is---" +cleanedurls);
					if(incomingUrl.replaceAll("http://", "").replaceAll("https://", "").replaceAll("www.", "").replaceAll("www", "").replaceAll("/", "").contains(cleanedurls.substring(1, cleanedurls.length()-1)))
					{
						//System.out.println("========URL AT 622 HOST FOUND ======" + urlParamsMap.get(cleanedurls));
						uparams.add(urlParamsMap.get(cleanedurls));
					}
				}
				
				
			}

			ArrayList<String> contentarray = new ArrayList<>();
			String [] UnRxlist = "".split(",");
			String boilerpipe = "";
			for(URLParams tempup : uparams)
			{
					String divClass = tempup.getDivClass();
					String unwantedRegex = tempup.getUnwantedRegex();
					//System.out.println("\n\n divClass======"+ divClass);		
					boilerpipe = tempup.getBoilerpipe();
					//System.out.println("\n\n=============="+boilerpipe+"===============\n\n\n");
					if(boilerpipe.equals("T"))
					{
						final HTMLDocument boilerhtmlDoc = HTMLFetcher.fetch(new URL(incomingUrl));
						final TextDocument boilerdoc = new BoilerpipeSAXInput(boilerhtmlDoc.toInputSource()).getTextDocument();
						UnRxlist = unwantedRegex.split("@@!!");
						String boilercontent = CommonExtractors.ARTICLE_EXTRACTOR.getText(boilerdoc);
						//System.out.println("=============Boilerpipe output==================="+boilercontent);
						for (String t : UnRxlist)
						{
							//System.out.println("\n\n+++++++++"+t+"++++++++\n\n");
							boilercontent = boilercontent.replaceAll(t,"");
						}
						contentarray.add(boilercontent);
					}
					else
					{
						//System.out.println("\n\n\n\n divClass ::: "+divClass+"\n\n\n\n");
						//System.out.println("\n\n\n\n divClass ::: "+unwantedRegex+"\n\n\n\n");
						if(!unwantedRegex.equals("null"))
							UnRxlist = unwantedRegex.split("@@@@!!");
						
						if (doc.getElementsByAttributeValue("class", divClass) != null && doc.getElementsByAttributeValue("class", divClass).isEmpty() == false)
						{
							String temp = doc.getElementsByAttributeValue("class", divClass)+" ";
							//System.out.println("========PROCESSED CONTENT else ACC TO CLASS IS 619 1======" + temp +"***");
							if(!unwantedRegex.equals("null") && UnRxlist.length >=2)
							{
								//System.out.println("\n\n++++++++++++"+UnRxlist[0]+"++++++++++\n\n");
								for (String t : UnRxlist[0].split("@@!!"))
									temp = temp.replaceAll(t,"");
							}
							//System.out.println("========PROCESSED CONTENT else ACC TO CLASS IS 619 2======" + temp +"***");
														
							Document test = Jsoup.parse(temp);
							Elements paragraphs = test.select("p");
							for(org.jsoup.nodes.Element p : paragraphs)
								content = content + "\n" + p.text();
							//System.out.println("========PROCESSED CONTENT else ACC TO CLASS IS 619======" + content+"***");
							if(content.equals(""))
							{
								test.select("br").append("\\n");
								test.select("p").prepend("\\n\\n");
								//System.out.println("========PROCESSED CONTENT else ACC TO CLASS IS 619 3======" + test.text() +"***");
								contentarray.add(test.text().replace("\\n", "\n"));
								//System.out.println("========PROCESSED CONTENT else ACC TO CLASS IS 619 4======" + contentarray.get(0) +"***");
							}else
								contentarray.add(content);
						}
						else if (doc.getElementsByAttributeValue("id", divClass) != null && doc.getElementsByAttributeValue("id", divClass).isEmpty() == false) 
						{
							String test = doc.getElementsByAttributeValue("id", divClass)+" ";
							if(!unwantedRegex.equals("null") && UnRxlist.length >=2)
							{
								//System.out.println("\n\n++++++++++++"+UnRxlist[0]+"++++++++++\n\n");
								for (String t : UnRxlist[0].split("@@!!"))
									test = test.replaceAll(t,"");
							}
							Document tes1 = Jsoup.parse(test);
							Elements paragraphs = tes1.select("p");
							for(org.jsoup.nodes.Element p : paragraphs)
								content = content + "\n" + p.text();
							//System.out.println("========PROCESSED CONTENT ACC TO ID IS 619======" + content);
							if(content.equals(""))
							{
								tes1.select("br").append("\\n");
								tes1.select("p").prepend("\\n\\n");
								////System.out.println(document.text().replace("\\n", "\n"));
								contentarray.add(tes1.text().replace("\\n", "\n"));
							}else
								contentarray.add(content);
						} else if (doc.getElementsByAttributeValue("itemprop", divClass) != null && doc.getElementsByAttributeValue("itemprop", divClass).isEmpty() == false) 
						{
							String test = doc.getElementsByAttributeValue("itemprop", divClass)+" ";
							if(!unwantedRegex.equals("null") && UnRxlist.length >=2)
							{
								//System.out.println("\n\n++++++++++++"+UnRxlist[0]+"++++++++++\n\n");
								for (String t : UnRxlist[0].split("@@!!"))
									test = test.replaceAll(t,"");
							}
							Document tes1 = Jsoup.parse(test);
							Elements paragraphs = tes1.select("p");
							for(org.jsoup.nodes.Element p : paragraphs)
								content = content + "\n" + p.text();
							//System.out.println("========PROCESSED CONTENT ACC TO ID IS 619======" + content);
							if(content.equals(""))
							{
								tes1.select("br").append("\\n");
								tes1.select("p").prepend("\\n\\n");
								contentarray.add(tes1.text().replace("\\n", "\n"));
							}else
								contentarray.add(content);
						}
						else if (content.toLowerCase().contains("ad blocker")) 
						{
							BufferedWriter bfw = new BufferedWriter(new FileWriter(new File(CrawlHubsFile), true));
							bfw.write(incomingUrl + "\n");
							bfw.flush();
							bfw.close();
							content = "#HUBPAGE\t" + content;
						}
				}
					
			}

			
			// //System.out.println("Processed Content is======"+content) ;
			if(!contentarray.isEmpty() || contentarray.size()!=0)
			{
				if(!contentarray.get(0).equals(""))
				{
					String [] array = contentarray.get(0).trim().split(" ");

        			if(array.length==1){
           				BufferedWriter bfw = new BufferedWriter(new FileWriter(new File(CrawlHubsFile), true));
						bfw.write(incomingUrl + "\n");
						bfw.flush();
						bfw.close();
						BufferedWriter bfw2 = new BufferedWriter(new FileWriter(new File(GlobalHubsFile), true));
						bfw2.write(incomingUrl + "\n");
						bfw2.flush();
						bfw2.close();
						return contentarray.get(10);
        			}	
					String content_mithran = contentarray.get(0);
					////System.out.println("\n\n\n"+content_mithran+"\n\n\n");
					if(UnRxlist.length >=2 && boilerpipe.equals("F"))
					{
						//System.out.println("\n\n\n"+UnRxlist[1]+"\n\n\n");
						for(String t : UnRxlist[1].split("@@!!"))
						{
							//System.out.println("\n\n\n++++++++++++++++"+t+"+++++++++++++++\n\n\n");
							content_mithran = content_mithran.replaceAll(t,"");
						}
						return content_mithran;
					}
					else
					////System.out.println("\n\n\n\n========Content Mithran======\n\n\n\n" + content_mithran +"***");
					return content_mithran;
				}
				else
				{
					BufferedWriter bfw = new BufferedWriter(new FileWriter(new File(CrawlHubsFile), true));
					bfw.write(incomingUrl + "\n");
					bfw.flush();
					bfw.close();
					BufferedWriter bfw2 = new BufferedWriter(new FileWriter(new File(GlobalHubsFile), true));
					bfw2.write(incomingUrl + "\n");
					bfw2.flush();
					bfw2.close();
					return contentarray.get(10);
				}
			}
			else{
                        BufferedWriter bfw = new BufferedWriter(new FileWriter(new File(CrawlHubsFile), true));
						bfw.write(incomingUrl + "\n");
						bfw.flush();
						bfw.close();
						BufferedWriter bfw2 = new BufferedWriter(new FileWriter(new File(GlobalHubsFile), true));
						bfw2.write(incomingUrl + "\n");
						bfw2.flush();
						bfw2.close();
				return contentarray.get(0);
                        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

	/*
	 * by pragya- to split content on basis of paragraphs or large segments of
	 * content
	 */
	public static String processByParagraph(String content, String title) {
		// process on basis of frequency of words in paragraphs
		// threshold is taken as frequency of words in title

		String[] paragraphs = content.split(System.getProperty("line.separator"));
		int cnt = 0;
		int index1 = 0;

		//System.out.println("para count" + paragraphs.length);
		content = "";
		for (String paraStr : paragraphs) {
			// title.length()
			if (paraStr.length() > 68) {
				content = content + paraStr + "\n";
			}
		}
		return content;
	}

	
	public Metadata addFieldsToMetadata(Map<String, URLParams> urlParamsMap, String incomingUrl,Document doc, Metadata metadata) throws URISyntaxException, IOException
	{
		String host = "";
		String hostName="";
		String idCode="";
		String resourceType="";
		String identifier="";
		String dateClass="";
		String publishedDate="";
		ArrayList<URLParams> up= new ArrayList<URLParams>();
		URL u_modified = IdUtils.getUrl(incomingUrl);
		String cleanedurl = IdUtils.cleanUrls(u_modified);
		
				
		try
		{
			host = new URL(incomingUrl).getHost();
			//System.out.println("========HOST AT 614======" + host);
			
			for(String url:CleanedUrlMap.keySet())
			{
				////System.out.println("========URL AT 618======" + url+ "cleaned url is---" +CleanedUrlMap.get(url));
				for(String cleanedurls:CleanedUrlMap.get(url))
				{
					////System.out.println("========URL AT 619swathi======" + cleanedurl+ "cleaned url is---" +cleanedurls);
					if(incomingUrl.replaceAll("http://", "").replaceAll("https://", "").replaceAll("www.", "").replaceAll("www", "").replaceAll("/", "").contains(cleanedurls))
					{
						//System.out.println("========URL AT 622 HOST FOUND ======" + urlParamsMap.get(cleanedurls));
						up.add(urlParamsMap.get(cleanedurls));
					}
				}
				
				
			}
			
			
			////System.out.println("========URLParams OBJECT AT 610======" + up.toString());
			
			
			
			for(URLParams tempup : up)
			{
				
					idCode=tempup.getIdcode();
					
					
					resourceType=tempup.getResourceType();
					
					identifier=resourceType+"_" + idCode;
					
					hostName=tempup.getNewsSource();
					dateClass=tempup.getClassForDate();
					if(!PubDatefetched)
					{
						if (doc.getElementsByAttributeValue("class", dateClass) != null && doc.getElementsByAttributeValue("class", dateClass).isEmpty() == false) {
							publishedDate = doc.getElementsByAttributeValue("class", dateClass).text();
							publishedDate = dateFormatter(publishedDate);
							//System.out.println("========PROCESSED DATE FROM CLASS IS 617======" + publishedDate);
							publishedDate=sdf.format(useNattyParser(publishedDate));
							//System.out.println("========PROCESSED DATE NATTY PARSER IS 619======" + publishedDate);
											
						}
						else
						{
							publishedDate = sdf.format(useNattyParser(doc.getElementsByTag("body").text()));
							//System.out.println("========PROCESSED DATE NATTY PARSER IS 619mithran======" + publishedDate);
						}
					}
					else
					{
						//System.out.println("\n\n\n\n\n**** "+PubDate+" ****\n\n\n\n\n");
						publishedDate = dateFormatter(PubDate.get(0));
						//System.out.println("\n\n\n\n\n"+publishedDate+"\n\n\n\n\n");
						publishedDate = sdf.format(useNattyParser(publishedDate));
					}					
					
				
				
				identifier=identifier+"_"+sdf.format(new Date());
				//Check whether the identifed publish date is after TODAY's date
				publishedDate = checkDateModule(publishedDate);
			
				System.out.println("HOST         :" + host);	
				System.out.println("IDCODE       :" + idCode);
				System.out.println("HOSTNAME     :" + hostName);
				//System.out.println("========RESOURCETYPE AT 608======" + resourceType);
				//System.out.println("========IDENTIFIER AT 608======" + identifier);
				System.out.println("PUBLISHDATE  :" + publishedDate);
			
			
			
				metadata.add("idcode", idCode);
				metadata.add("hostname", hostName);
				metadata.add("resourceType", resourceType);
				metadata.add("baseUrl", host);
				metadata.add("identifier", identifier); 
				metadata.add("publishdate", publishedDate);
			
				return metadata;
			}
			
		} catch (Exception e) {
			
			IdUtils.getIdentifier(cleanedurl, incomingUrl, getConf());
			
			idCode=IdUtils.IDCODE;
			if(idCode==null)
				idCode="NOTFND";
			
			
			
			identifier=IdUtils.IDENTIFIER;
			
			resourceType=IdUtils.RESOURCETYPE;
			
			IdUtils.getHostname(resourceType, u_modified, getConf());
			
			hostName=IdUtils.IDENTIFIED_HOSTNAME;
			
			if(hostName==null)
				hostName=host;
			
			publishedDate = sdf.format(useNattyParser(doc.getElementsByTag("body").text()));
			identifier=identifier+"_"+sdf.format(new Date());
			//Check whether the identifed publish date is after TODAY's date
			publishedDate = checkDateModule(publishedDate);
			
			//System.out.println("========HOST AT 685======" + host);
			//System.out.println("========IDCODE AT 686======" + idCode);
			//System.out.println("========HOSTNAME AT 687======" + hostName);
			//System.out.println("========RESOURCETYPE AT 688======" + resourceType);
			//System.out.println("========IDENTIFIER AT 689======" + identifier);
			//System.out.println("========PUBLISHDATE AT 690======" + publishedDate);
			
			
			
			metadata.add("idcode", idCode);
			metadata.add("hostname", hostName);
			metadata.add("resourceType", resourceType);
			metadata.add("baseUrl", host);
			metadata.add("identifier", identifier); 
			metadata.add("publishdate", publishedDate);
			
			
			return metadata;
		}
		return null;	
	}
	
	/*
	 * by pragya- to extract title
	 */
	public static String getProcessedTitle(Map<String, URLParams> urlParamsMap, String incomingUrl, Document doc) {
		////System.out.println("IN GET PROCESSED ARTICLE FUNCTION");
		String host = "";
		String title = doc.title();
		String titleRegex = "";
		boolean flagForMatch = true;
		ArrayList<URLParams> up= new ArrayList<URLParams>();
		try {
			host = new URL(incomingUrl).getHost();
			//URLParams up = urlParamsMap.get(host);
			
			for(String url:CleanedUrlMap.keySet())
			{
				////System.out.println("========URL AT 618======" + url+ "cleaned url is---" +CleanedUrlMap.get(url));
				for(String cleanedurls:CleanedUrlMap.get(url))
				{
					////System.out.println("========URL AT 619swathi======" + url+ "cleaned url is---" +cleanedurls);
					if(incomingUrl.replaceAll("http://", "").replaceAll("https://", "").replaceAll("www.", "").replaceAll("www", "").replaceAll("/", "").contains(cleanedurls))
					{
						//System.out.println("========URL AT 622 HOST FOUND ======" + urlParamsMap.get(cleanedurls));
						up.add(urlParamsMap.get(cleanedurls));
					}
				}
				
				
			}
			boolean flagtitleRegex = false;
			for(URLParams tempup : up)
			{
				if (tempup != null) {
					String articleEnds = tempup.getArticleEndsWith();
					String titleClass = tempup.getTitleClass();
					if(!flagtitleRegex)
					{
						titleRegex = tempup.getTitleRegex();
						flagtitleRegex = true;
					}
					////System.out.println("ARTICLE ENDS WITH===>" + articleEnds);
					if(!titleClass.contains("@@!!"))
					{
						if (flagForMatch == true) {
							////System.out.println("=====Article Pattern found=====");
							if (doc.select(titleClass.toLowerCase()).first().text() != null && doc.select(titleClass.toLowerCase()).text().isEmpty() == false) {
								title = doc.select(titleClass.toLowerCase()).first().text();
								break;
								////System.out.println("PROCESSED TITLE H1===>" + title);
							} else if (doc.getElementsByAttributeValue("class", titleClass).text() != null && doc.getElementsByAttributeValue("class", titleClass).text().isEmpty() == false) {
								title = doc.getElementsByAttributeValue("class", titleClass).first().text();
								break;
								////System.out.println("PROCESSED TITLE ==>" + title + " FOR CLASS==>" + titleClass);
							} else if (doc.getElementsByAttributeValue("id", titleClass).text() != null && doc.getElementsByAttributeValue("id", titleClass).text().isEmpty() == false) {
								title = doc.getElementsByAttributeValue("id", titleClass).first().text();
								break;
								////System.out.println("PROCESSED TITLE==>" + title + " FOR ID==>" + titleClass);
							} /* added by pragya on 21st june 2017 */
							else if (doc.getElementsByAttributeValue("itemprop", titleClass).text() != null && doc.getElementsByAttributeValue("itemprop", titleClass).text().isEmpty() == false) {
								title = doc.getElementsByAttributeValue("itemprop", titleClass).first().text();
								break;
								////System.out.println("PROCESSED TITLE==>" + title + " FOR ID==>" + titleClass);
							}
						}
					}else
					{
		
						String[] temp = titleClass.split("@@!!");
						String divClass = temp[0];
						String intitleClass = temp[1];
						if(doc.getElementsByAttributeValue("itemprop", divClass).text()!=null && doc.getElementsByAttributeValue("itemprop", divClass).text().isEmpty() == false)
						{
							String test = doc.getElementsByAttributeValue("itemprop", divClass)+" ";
							Document tes1 = Jsoup.parse(test);
							if (tes1.select(intitleClass.toLowerCase()).first().text() != null && tes1.select(intitleClass.toLowerCase()).text().isEmpty() == false) {
								title = tes1.select(intitleClass.toLowerCase()).first().text();
								break;
							}
						}

						if(doc.getElementsByAttributeValue("id", divClass).text()!=null && doc.getElementsByAttributeValue("id", divClass).text().isEmpty() == false )
						{
							String test = doc.getElementsByAttributeValue("id", divClass)+" ";
							Document tes1 = Jsoup.parse(test);
							if (tes1.select(intitleClass.toLowerCase()).first().text() != null && tes1.select(intitleClass.toLowerCase()).text().isEmpty() == false) {
								title = tes1.select(intitleClass.toLowerCase()).first().text();
								break;
							}
						}

						if(doc.getElementsByAttributeValue("class", divClass).text()!=null && doc.getElementsByAttributeValue("class", divClass).text().isEmpty() == false )
						{
							String test = doc.getElementsByAttributeValue("class", divClass)+" ";
							Document tes1 = Jsoup.parse(test);
							if (tes1.select(intitleClass.toLowerCase()).first().text() != null && tes1.select(intitleClass.toLowerCase()).text().isEmpty() == false) {
								title = tes1.select(intitleClass.toLowerCase()).first().text();
								break;
							}
						}

					}

				} else if (incomingUrl.contains("indiatimes")) {
					title = doc.select("h1").first().text();
					break;
				}
			}


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("\n\n+++++++++"+titleRegex+"++++++++\n\n");
	
		for (String t : titleRegex.split("@@!!"))
		{
			//System.out.println("\n\n++++wd+++++"+t+"+++wdf+++++\n\n");
			title = title.replaceAll(t,"");
		}
		return title;
	}

	/* Date module */

	private static String getdate(String content, String datePattern) {

		String date = "";
		try {
			Pattern pattern = Pattern.compile(datePattern);

			Matcher matcher = pattern.matcher(content);
			// check all occurance
			while (matcher.find()) {
				// //System.out.println("Level.INFO,"Group find");
				if (date.length() < matcher.group().length())
					date = matcher.group();
			}
			/*
			 * for(int i=0;i<matcher.groupCount();i++) {
			 * //System.out.println("Level.INFO,"Mathces : "+matcher.group(i)); }
			 */
			// //System.out.println("Level.INFO,"date: " + date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	private static String getDateString(String content) {
		String[] paragraphs = content.split(System.getProperty("line.separator"));
		String dateStr = "";
		int cnt = 0;
		int index1 = 0;
		String patt1 = "\\b(PTI|update:|Update:|updated:|updated|Updated:|Updated|Published|Published:|published:|published|अपडेटेड:|अपडेटेड)\\b";
		Pattern pattern1 = Pattern.compile(patt1);

		Matcher matcher = pattern1.matcher(content);
		if (matcher.find()) {
			String matchresult = matcher.group();
			dateStr = content.substring(content.indexOf(matchresult), content.indexOf(matchresult) + 60);
		}
		return dateStr;
	}

	private String formatDatePattern(String publishedDate) {

		if (publishedDate.contains("|")) {
			publishedDate = publishedDate.replace("|", "");

		}
		if (publishedDate.contains("अपडेटेड:") || publishedDate.contains("अपडेटेड :")) {
			publishedDate = publishedDate.replaceAll("अपडेटेड:", "").replaceAll("अपडेटेड :", "");
		}
		publishedDate = publishedDate.replaceAll("\\b , \\b", "").replaceAll("\\b at \\b", "")
				.replaceAll("\\b - \\b", " ").replaceAll(",", " ").replaceAll("at", " ").replaceAll("\'st", "")
				.replaceAll("\'th", "").replaceAll("th", "").replaceAll("\'rd", "").replaceAll("rd", "")
				.replaceAll("\'", "").replaceAll("[(]", "").replaceAll("[)]", "");
		publishedDate = publishedDate.replace("  ", " ");

		// For converting hindi month to english month
		if (publishedDate.contains("जनवरी")) {
			publishedDate = publishedDate.replace("जनवरी", "January");

		} else if (publishedDate.contains("फ़रवरी")) {
			publishedDate = publishedDate.replace("फ़रवरी", "February");

		} else if (publishedDate.contains("फरवरी")) {
			publishedDate = publishedDate.replace("फरवरी", "February");
		} else if (publishedDate.contains("मार्च")) {
			publishedDate = publishedDate.replace("मार्च", "March");
		} else if (publishedDate.contains("अप्रैल")) {
			publishedDate = publishedDate.replace("अप्रैल", "April");
		} else if (publishedDate.contains("मई")) {
			publishedDate = publishedDate.replace("मई", "May");
		} else if (publishedDate.contains("जून")) {
			publishedDate = publishedDate.replace("जून", "June");
		} else if (publishedDate.contains("जुलाई")) {
			publishedDate = publishedDate.replace("जुलाई", "July");
		} else if (publishedDate.contains("आगस्त")) {
			publishedDate = publishedDate.replace("आगस्त", "August");
		} else if (publishedDate.contains("ऑगस्ट")) {
			publishedDate = publishedDate.replace("ऑगस्ट", "August");
		} else if (publishedDate.contains("अगस्त")) {
			publishedDate = publishedDate.replace("अगस्त", "August");
		} else if (publishedDate.contains("सप्टेंबर")) {
			publishedDate = publishedDate.replace("सप्टेंबर", "September");
		} else if (publishedDate.contains("सितम्बर")) {
			publishedDate = publishedDate.replace("सितम्बर", "September");
		} else if (publishedDate.contains("अक्टूबर")) {
			publishedDate = publishedDate.replace("अक्टूबर", "October");
		} else if (publishedDate.contains("अकतूबर")) {
			publishedDate = publishedDate.replace("अकतूबर", "October");
		} else if (publishedDate.contains("नवेम्बर")) {
			publishedDate = publishedDate.replace("नवेम्बर", "November");
		} else if (publishedDate.contains("नवम्बर")) {
			publishedDate = publishedDate.replace("नवम्बर", "November");
		} else if (publishedDate.contains("दिसम्बर")) {
			publishedDate = publishedDate.replace("दिसम्बर", "December");
		} else {
			// Leave
		}

		//System.out.println("String encountered for date formatting: " + publishedDate);
		return publishedDate;

		/* Till here */
	}

	private Date useNattyParser(String textinput) {
		if (nattyParser == null)
			nattyParser = new NattyParser(TimeZone.getTimeZone("Asia/Kolkata"));
		List<DateGroup> groups = nattyParser.parse(textinput);
		List<Date> dates = new ArrayList<>();
		for (DateGroup group : groups) {
			dates = group.getDates();
			int line = group.getLine();
			int column = group.getPosition();
			String matchingValue = group.getText();
			String syntaxTree = group.getSyntaxTree().toStringTree();
			Map<String, List<ParseLocation>> parseMap = group.getParseLocations();
			boolean isRecurreing = group.isRecurring();
			Date recursUntil = group.getRecursUntil();

		}
		if(!dates.isEmpty())
			return dates.get(0);
		else
			return new Date();
	}

	public String callDateModule(Document doc, String datePattern, String url) {
		if (nattyParser == null)
			nattyParser = new NattyParser(TimeZone.getTimeZone("Asia/Kolkata"));

		String publishedDate = "";

		try {
			String dateString = getDateString(doc.body().text());

			if (dateString == null || dateString != "") {
				//System.out.println("Datestring found: " + dateString);
				publishedDate = getdate(dateString, datePattern);
				//System.out.println("Datestring found: " + dateString + " || pattern match at :" + publishedDate);
			}
			if (publishedDate.equals("") || (publishedDate == null) || (publishedDate.length() <= 13)) {
				//System.out.println("Datestring match failed,trying body match");
				publishedDate = getdate(doc.getElementsByTag("body").text(), datePattern);
				//System.out.println("Call date module for published date in whole body : " + publishedDate);

			}

			if (!publishedDate.equals("")) {
				publishedDate = formatDatePattern(publishedDate);
				//System.out.println("Using Natty parser on identified date pattern");
				publishedDate = sdf.format(useNattyParser(publishedDate));
			} else {

				//System.out.println("No identified date pattern, using natty parser directly");
				publishedDate = sdf.format(useNattyParser(doc.getElementsByTag("body").text()));

			}
		} catch (Exception e) {
			//System.out.println("All is failing, using date pattern recognization from natty: ");

			publishedDate = sdf.format(useNattyParser(doc.getElementsByTag("body").text()));
		}

		
		// Final publishedDate will be of format: Tue Jun 27 17:57:57 IST 2017
		// i.e.

		// Check whether the identifed punlish date is after TODAY's date
		publishedDate = checkDateModule(publishedDate);

		
		//System.out.println("Final publishdate from dateModule:" + publishedDate);
		return publishedDate;
	}

	public String checkDateModule(String identifieddate) {

		Date currentdate = new Date();
		try {
			if (currentdate.before(sdf.parse(identifieddate))) {
				return sdf.format(currentdate);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return identifieddate;

	}

	private LinkedHashSet<String> getNERPhraseListFromFile(String filePath) {
		LinkedHashSet<String> NERList = new LinkedHashSet<>();
		String phrase = "";
		try {

			BufferedReader br = new BufferedReader(new FileReader(new File(filePath)));

			while ((phrase = br.readLine()) != null) {
				NERList.add(phrase.toLowerCase());
			}
			return NERList;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return NERList;
	}

	private LinkedHashSet<String> getNERPhraseListFromContent(String text) {
		LinkedHashSet<String> NERPhraseList = new LinkedHashSet<>();
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		/* create an empty Annotation just with the given text */
		Annotation document = new Annotation(text);

		/* run all Annotators on this text */
		pipeline.annotate(document);
		// a CoreMap is essentially a Map that uses class objects as keys and
		// has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		List<String> tokens = new ArrayList();

		StringBuilder sb = new StringBuilder();
		for (CoreMap sentence : sentences) {
			// traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			// //System.out.println("sentence is
			// ***"+sentence.get(CoreAnnotations.PartOfSpeechAnnotation.class))
			// ;
			String prevNeToken = "O";
			String currNeToken = "O";
			boolean newToken = true;

			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {

				currNeToken = token.get(NamedEntityTagAnnotation.class);
				// this is the text of the token
				String word = token.get(TextAnnotation.class);
				// this is the POS tag of the token
				String pos = token.get(PartOfSpeechAnnotation.class);
				// this is the NER label of the token
				String ne = token.get(NamedEntityTagAnnotation.class);

				if (currNeToken.equals("O")) {

					if (!prevNeToken.equals("O") && (sb.length() > 0)) {
						handleEntity(prevNeToken, sb, tokens);
						newToken = true;
					}
					continue;
				}
				if (newToken) {
					prevNeToken = currNeToken;
					newToken = false;

					sb.append(word);
					continue;
				}

				if (currNeToken.equalsIgnoreCase("PERSON") || currNeToken.equalsIgnoreCase("ORGANIZATION")
						|| currNeToken.equalsIgnoreCase("LOCATION") && currNeToken.equals(prevNeToken)) {

					sb.append(" " + word);
				} else {

					handleEntity(prevNeToken, sb, tokens);
					newToken = true;
				}
				prevNeToken = currNeToken;

				//System.out.println("NER tokens are*****" + sb.toString());
				if (!sb.toString().isEmpty() || sb.toString().equalsIgnoreCase(" ")) {
					NERPhraseList.add(sb.toString().toLowerCase());
				}

			}

		}
		//System.out.println("Final NER List is*****" + NERPhraseList.toString());

		return NERPhraseList;
	}

	private static void handleEntity(String inKey, StringBuilder inSb, List inTokens) {

		inTokens.add(new EmbeddedToken(inKey, inSb.toString()));
		inSb.setLength(0);
	}
}

class EmbeddedToken {

	private String name;
	private String value;

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public EmbeddedToken(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	@Override
	public String toString() {
		return "EmbeddedToken [name=" + name + ", value=" + value + "]";
	}

}
